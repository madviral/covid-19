import i18n from "i18next";
import cookie from 'js-cookie'
import { initReactI18next } from "react-i18next";
import ru from './assets/locales/ru';
import kz from './assets/locales/kz';

// the translations
// (tip move them in a JSON file and import them)
const resources = {
    ru,
    kz
};

const LANGUAGE = cookie.get('next-i18next') || 'ru';

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        lng: LANGUAGE,
        fallbackLng: 'ru',
        whitelist: ['ru', 'kz'],
        cleanCode: true,
        ns: ['common'],
        defaultNS: 'common',
        fallbackNS: 'common',

        keySeparator: false, // we do not use keys in form messages.welcome

        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });

export default i18n;
