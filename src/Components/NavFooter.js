import React from 'react'
import clsx from "clsx"
import { withTranslation } from "react-i18next";
import { NavLink, withRouter } from "react-router-dom";
import CloseIcon from '../icons/CloseIcon'

const withSource = (path, source) =>
  source
    ? `${path}?source=${source}`
    : `${path}`;

const NavFooter = ({ location, t, history }) => {
  const query = new URLSearchParams(location.search);
  const source = query.get("source");
  const leisureIsOpen = location.pathname.indexOf("leisure") !== -1;
  const brainWarsIsOpen = location.pathname.indexOf("brain-wars") !== -1;
  const chatIsOpen = location.pathname.indexOf("chat") !== -1;
  if (chatIsOpen) {
    return null
  }

  const onButtonClick = () => {
    !leisureIsOpen ? history.push('/leisure') : history.push('/')
  };

  if (source === "ios" || brainWarsIsOpen) {
    return null;
  }

  return (
    <nav className="mobile-nav">
      <div className="mobile-nav__first-half">
        <NavLink className="mobile-nav__link --map-kz" to={withSource('/', source)} exact>
          { t('country') }
        </NavLink>
        <NavLink className="mobile-nav__link --info" to={withSource('/news', source)}>
          { t('info') }
        </NavLink>
      </div>
      <button className={clsx("mobile-nav-button", leisureIsOpen && "--leisure-is-open")} onClick={onButtonClick}>
        <p className="mobile-nav-button__text">
          { t('leisureFromVLife') }
        </p>
        <CloseIcon className="mobile-nav-button__close-icon" />
      </button>
      <div className="mobile-nav__second-half">
        <NavLink className="mobile-nav__link --graphs" to={withSource('/graphs', source)} exact>
          { t('infographics') }
        </NavLink>
        <NavLink className="mobile-nav__link --contacts" to={withSource('/contacts', source)} exact>
          { t('contacts') }
        </NavLink>
      </div>
    </nav>
  );
};

export default withTranslation()(withRouter(NavFooter));
