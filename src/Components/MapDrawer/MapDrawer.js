import React, { useEffect, useState } from "react";
import clsx from "clsx";
import CloseIcon from "../../icons/CloseIcon";
import { getDateWithTime } from "../../utils";
import i18n from "i18next";
import { withTranslation } from "react-i18next";
import DG from "2gis-maps";
import "./styles.css";

const MapDrawer = ({
  isOpen,
  handleToggleDrawer,
  t,
  currentCity,
  places,
  mapZoom,
  getRadius,
  statistic,
  createClusterCustomIcon
}) => {
  const [map, setMap] = useState();
  useEffect(() => {
    DG.then(function() {
      return DG.plugin(
        "https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"
      );
    }).then(function() {
      const map = DG.map("map-drawer", {
        zoom: 9,
        fullscreenControl: false,
        zoomControl: false,
      });
      setMap(map);
    });
  }, []);
  useEffect(() => {
    const hascurrentCity = currentCity && typeof currentCity.id !== "undefined";
    if (hascurrentCity && isOpen) {
      map.setZoom(mapZoom).setView([currentCity.latitude, currentCity.longitude]);
      const markers = DG.markerClusterGroup({
        iconCreateFunction: createClusterCustomIcon,
        maxClusterRadius: 40
      });
      for (let i = 0; i < places.length; i++) {
        const a = places[i];
        const markerIcon = DG.divIcon({
          className: "my-custom-icon",
          iconSize: getRadius(a.infected),

          html: `<span class="icon-number">${a.infected}</span>`
        });
        const marker = DG.marker([a.latitude, a.longitude], {
          icon: markerIcon,
          title: a.infected
        });
        marker.bindPopup(
          `  
      <b>${a.name}</b> <br />
      <span style="color:#eb5757">
        ${t("detected")}: ${a.infected}
      </span>
      <br />
      <span style="color:#219653" >
      ${t("healed")}: ${a.recovered}
      </span>
      <br />
      <span style="color:#828282" >
      ${t("dead")}: ${a.deaths}
      </span>
      <br />
      <small>
        ${t("updated")}:
        ${a.updated && getDateWithTime(a.updated, i18n.language)}
      </small>
    `,
          { sprawling: true }
        );
        if (a.infected > 0) {
          markers.addLayer(marker);
        }
      }
      map.addLayer(markers);
    }
  }, [currentCity, isOpen]);

  return (
    <div className={clsx("map-drawer", isOpen && "--map-drawer-is-open")}>
      <div className="drawer-page-statistics">
        <div className="page_statistics --drawer">
          <div className="statistic --red">
            <div className="stat__title">{t("detected")}</div>
            <div className="stat__count">{statistic.total}</div>
          </div>
          <div className="statistic --green">
            <div className="stat__title">{t("healed")}</div>
            <div className="stat__count">{statistic.recovered}</div>
          </div>
          <div className="statistic --gray">
            <div className="stat__title">{t("dead")}</div>
            <div className="stat__count">{statistic.deaths}</div>
          </div>
        </div>
      </div>
      
      <div id="map-drawer" className="map-drawer-full" />
      <button onClick={handleToggleDrawer} className="map-drawer__close-button">
        <CloseIcon />
      </button>
    </div>
  );
};

MapDrawer.defaultProps = {
  mapZoom: 9,
}

export default withTranslation()(MapDrawer);
