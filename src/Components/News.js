import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import { withTranslation } from "react-i18next";
import clsx from 'clsx'
import info from '../api/info'
import Expander from "./Expander";
import NewsFeed from './news/Feed'
import NewsList from './news/List'
import diagnosticsStub from './news/diagnosticStub'

const NEWS = 'news'
const INFO = 'info'
const DIAGNOSTICS = 'diagnostics'

const News = ({time, i18n, t, location }) => {
  const newsPath = `/${NEWS}`
  const infoPath = `/${NEWS}/${INFO}`
  const diagnosticsPath = `/${NEWS}/${DIAGNOSTICS}`
  const locale = i18n.language || 'ru'

  return (
    <div className="news page">
      <div className="container">
        <div className="date-wrapper">
          <div className="date-title">{time}</div>
        </div>
        <div className="title news__title">
          <NavLink
            exact
            to={newsPath}
            className={clsx("news__title-link", '--news')}
          >
            {t("news")}
          </NavLink>
          <NavLink
            to={infoPath}
            className={clsx("news__title-link", '--info')}
          >
            {t("useful")}
          </NavLink>
          <NavLink
            to={diagnosticsPath}
            className={clsx("news__title-link", '--diagnostics')}
          >
            {t("diagnostic")}
          </NavLink>
        </div>
        {location.pathname === newsPath && <NewsFeed />}
        {location.pathname === infoPath &&
          info.map(item => (
            <Expander
              key={item.id}
              title={item[locale].title}
              content={item[locale].content}
              className="news__expander"
            />
          ))}
        {location.pathname === diagnosticsPath && <NewsList news={diagnosticsStub} />}
      </div>
    </div>
  );
}

export default withTranslation()(withRouter(News))
