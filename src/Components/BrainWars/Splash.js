import React from 'react'
import { Link } from 'react-router-dom'
import BrainWarsLogo from '../../icons/BrainWarsLogo'
import Wrapper from './components/Wrapper'  
import './styles.css'

const BrainWars = ({ match }) => (
  <Wrapper className='--brain-wars'>
    <div className='game-page__logo'></div>
    <BrainWarsLogo />
    <Link to={`${match.path}/start`} className='btn --green --big-space game-page__presented-btn'>
      Играть
    </Link>
  </Wrapper>
)

export default BrainWars

