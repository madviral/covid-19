import React from "react";
import { Route, Switch } from "react-router-dom";
import Splash from "./Splash"
import Game from "./Game"

const BrainWars = ({ match }) => (
  <Switch>
    <Route
      path={`${match.url}`}
      exact
      component={Splash}
    />
    <Route
      path={`${match.url}/start`}
      exact
      component={Game}
    />
  </Switch>
)

export default BrainWars