import React from 'react'
import { Link } from 'react-router-dom'
import clsx from "clsx"
import CloseIcon from '../../../icons/CloseIcon'

const Wrapper = ({ className, children }) => (
  <div className={clsx('game-page', className)}>
    {children}
    <Link className={clsx('mobile-nav-button', '--leisure-is-open', 'game-page__close-btn')} to='/leisure'>
      <CloseIcon className='mobile-nav-button__close-icon' />
    </Link>
  </div>
)

Wrapper.defaultProps = {
  className: ''
}

export default Wrapper

