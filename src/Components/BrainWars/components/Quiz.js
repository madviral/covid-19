import React, { useState } from 'react'
import Timer from './Timer'

const initialState = {
  pointer: 0,
  score: 0,
  scorePointer: 0,
  qAnswer: null
}
const Quiz = ({ list, onFinish }) => {
  const [state, setState] = useState(initialState)
  const { pointer, score, qAnswer, scorePointer } = state
  const { answers, body } = list[pointer]

  const onNext = (newScore, newScorePointer) => {
    if (pointer < list.length - 1) {
      setState({
        qAnswer: null,
        score: newScore,
        pointer: newScorePointer,
        scorePointer: newScorePointer
      })
    } else {
      setState(initialState)
      onFinish({ score: newScore })
    }
  }
  const onAnswer = (index) => {
    const newScore = answers[index].isRight ? score + 1 : score
    const newScorePointer = pointer + 1
    setState({
      ...state,
      scorePointer: newScorePointer,
      qAnswer: index,
      score: newScore
    })
    setTimeout(() => {
      onNext(newScore, newScorePointer)
    }, 500)
  }

  return (
    <div className="quiz">
      <div className="quiz__state">
        <span className="quiz__q-number">
          {pointer + 1}&nbsp;вопрос
        </span>
        <span className="quiz__point --error">
          {scorePointer - score}
        </span>
        &nbsp;/&nbsp;
        <span className="quiz__point --success">{score}</span>
      </div>
      <h3 className="quiz__q">
        {body}
      </h3>

      <Timer pointer={pointer} onNext={() => onNext(score, pointer + 1)} />

      <div className="quiz__body">
        {
          answers.map((q, index) => (
            <div 
              key={q.id}
              onClick={() => onAnswer(index)}
              className={`quiz__item 
                ${index === qAnswer ? q.isRight ? '--success' : '--error' : '' }`}
            >
              {q.body}
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default Quiz

