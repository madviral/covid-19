import React, { useState, useEffect } from 'react'
import axios from "../../../axiosConfig";
import { API } from "../../../utils";

const initialState = { list: [], place: null }
const Game = ({ fio, score, onRestart }) => {
  const [rating, setRating] = useState(initialState)
  useEffect(() => {
    const load = async () => {
      await axios({
        method: "POST",
        url: `${API}/v1/quiz/users`,
        data: {
          fio, score
        }
      })
      const { data } = await axios({
        method: "GET",
        url: `${API}/v1/quiz/users`,
      })
      setRating({
        list: data.slice(0, 10),
        place: data.findIndex(items => items.fio === fio) + 1
      })
    }
    load()
  }, [])

  return (
    <>
      <div className="game-page__result">
        {fio}, ваш результат: <span className="quiz__point --success">{score}</span>
      </div>

      <div className="game-page__rating">
        {
          rating.list.length > 0 && (
            <>
              {rating.place > 0 && <div className="game-page__rating-header">Вы на {rating.place} месте</div>}
              <div className="game-page__rating-subheader">{Math.min(rating.list.length, 10)} лучших:</div>
              {rating.list.map(person => (
                <div key={person.id} className='rating-person'>
                  <strong>{person.fio}</strong>
                  <span>{person.score}</span>
                </div>
              ))}
            </>
          )
        }
      </div>
      <button
        className="btn --green --big-space game-page__btn"
        onClick={onRestart}
      >
        Начать заново
    </button>
    </>
  )
}

export default Game

