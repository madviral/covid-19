import React, { useState, useEffect } from 'react'
import { useInterval } from '../utils/useInterval'

const step = 10
const Timer = ({ pointer, time, onNext }) => {
  const [curTime, setCurTime] = useState(0)

  useEffect(() => {
    setCurTime(0)
  }, [pointer])
  useInterval(
    () => {
      if (curTime === time) {
        onNext()
        setCurTime(0)
      } else {
        setCurTime(curTime + step)
      }
    },
    step
  )
  return (
    <div className="quiz__time">
      <span style={{ maxWidth: `${(curTime / time) * 100}%` }} />
    </div>
  )
}

Timer.defaultProps = {
  time: 15000
}

export default Timer

