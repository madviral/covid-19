import React from 'react'
import { withTranslation } from "react-i18next";

const BrainWars = ({ fioValue, onStart, onChangeFio, t }) => (
  <div className="game-page__form">
    <div className="disclaimer">
      <h4 className="disclaimer__title">
        Добро пожаловать в игру Битва умов!
      </h4>
      <p className="disclaimer__text">
        В этой игре вам предстоит ответить на 10 вопросов разной сложности и посмотреть, какое место вы займете среди остальных участников!
      </p>
    </div>
    <input type="hidden" value="autocomplete" />
    <input
      id="fio"
      type="text"
      autoComplete="off"
      className="form-control"
      name="fio"
      value={fioValue}
      onChange={({ target }) => onChangeFio(target.value)}
      placeholder={t("yourName")}
      required
    />
    <button
      className={`btn ${fioValue && '--green'} --big-space game-page__btn`}
      disabled={!fioValue}
      onClick={onStart}
    >
      {t('play')}
    </button>
  </div>
)

export default withTranslation()(BrainWars)

