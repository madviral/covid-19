import React, { useEffect, useState } from 'react'
import axios from "../../axiosConfig";
import { API } from "../../utils";
import Wrapper from './components/Wrapper'
import StartForm from './components/StartForm'
import Quiz from './components/Quiz'
import Result  from './components/Result'
import './styles.css'

const initialState = {
  fio: '',
  quiz: [{ answers: [], body: '' }],
  isStarted: false,
  score: 0,
  isFinished: false
}

const Game = () => {
  const [state, setState] = useState(initialState)

  useEffect(() => {
    const load = async () => {
      const response = await axios({
        method: "GET",
        url: `${API}/v1/quiz`
      })
      setState({ ...state, quiz: response.data })
    }
    load()
  }, [state.isFinished])

  return (
    <Wrapper>
      <div className='game-page__content'>
        <h2 className="game-page__header">
          Битва умов
        </h2>        
        {
          state.isFinished
            ? (
              <Result
                score={state.score}
                fio={state.fio}
                onRestart={() => setState({ ...initialState, fio: state.fio })}
              />
            )
            : !state.isStarted
              ? (
                <StartForm
                  fioValue={state.fio}
                  onChangeFio={value => setState({ ...state, fio: value })}
                  onStart={() => setState({ ...state, isStarted: true })}
                />
              ) : (
                <Quiz
                  list={state.quiz}
                  onFinish={({ score }) => setState({ ...state, score, isFinished: true })}
                />
              )
        }
      </div>
    </Wrapper>
  )
}

export default Game

