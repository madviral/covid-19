import React from "react";
import { withTranslation } from "react-i18next";
// import { getDateWithTime } from "../utils";

const Statistics = ({ cities, updated, t, i18n }) => (
  <article className="statistics">
    <h3 className="statistics__title">{t("kzStatisticsCovid19")}:</h3>
    <table className="table table-hover table-responsive-sm">
      <thead className="thead-light">
        <tr>
          <th scope="col"></th>
          <th scope="col">{t("infections")}</th>
          <th scope="col">{t("deaths")}</th>
          <th scope="col only-md">{t("healed")}</th>
        </tr>
      </thead>
      <tbody>
        {cities.map(item => (
          <tr key={item.id}>
            <th scope="row">{item.name}</th>
            <td>{item.infected}</td>
            <td>{item.deaths}</td>
            <td>{item.recovered}</td>
          </tr>
        ))}
      </tbody>
    </table>
    {/* <span className="statistics__updated text-muted">
      {t("updated")}: {updated && getDateWithTime(updated, i18n.language)}
    </span> */}
  </article>
);

Statistics.defaultProps = {
  cities: []
}

export default withTranslation()(Statistics);
