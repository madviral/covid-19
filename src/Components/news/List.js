import React from 'react'
import clsx from 'clsx'

const NewsList = ({ news }) => (
  <>
    {news.map(item => (
      <a
        href={item.url}
        className="news-item-link"
        rel="noopener noreferrer"
        target="_blank"
        key={item.id}
      >
        <section
          className={clsx("news-item", item.image && "--with-image")}
        >
          <div className="news-item__content">
            <h4 className="news-item__title">{item.title}</h4>
            <span className="news-item__resource">{item.resource}</span>
          </div>
          {item.image && (
            <div className="news-item__img-wrapper">
              <img
                className="img-fluid"
                src={item.image}
                alt={item.title}
              />
            </div>
          )}
        </section>
      </a>
    ))}
  </>
)

export default NewsList
