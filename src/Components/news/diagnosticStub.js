export default [
  {
    id: 1,
    title: "Резюме по COVID-19",
    resource: "medelement.com",
    url: "https://medelement.com/news/%D0%BA%D0%BE%D1%80%D0%BE%D0%BD%D0%B0%D0%B2%D0%B8%D1%80%D1%83%D1%81-covid-19-%D0%B4%D0%B8%D0%B0%D0%B3%D0%BD%D0%BE%D1%81%D1%82%D0%B8%D0%BA%D0%B0-%D0%B8-%D0%BB%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5/%D1%80%D0%B5%D0%B7%D1%8E%D0%BC%D0%B5-%D0%BF%D0%BE-covid-19",
    image: "https://files.medelement.com/uploads/co/33581381815050/miniature/3a5e95d80c023cc1426114f922a6b51b.png"
  },
  {
    id: 2,
    title: "Китай.Руководство по профилактике и лечению новой коронавирусной инфекции COVID- 19",
    resource: "medelement.com",
    url: "https://medelement.com/news/%D0%BA%D0%BE%D1%80%D0%BE%D0%BD%D0%B0%D0%B2%D0%B8%D1%80%D1%83%D1%81-covid-19-%D0%B4%D0%B8%D0%B0%D0%B3%D0%BD%D0%BE%D1%81%D1%82%D0%B8%D0%BA%D0%B0-%D0%B8-%D0%BB%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5/%D0%BA%D0%B8%D1%82%D0%B0%D0%B9-%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE-%D0%BF%D1%80%D0%BE%D1%84%D0%B8%D0%BB%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B5-%D0%B8-%D0%BB%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D1%8E-%D0%BD%D0%BE%D0%B2%D0%BE%D0%B9-%D0%BA%D0%BE%D1%80%D0%BE%D0%BD%D0%B0%D0%B2%D0%B8%D1%80%D1%83%D1%81%D0%BD%D0%BE%D0%B9-%D0%B8%D0%BD%D1%84%D0%B5%D0%BA%D1%86%D0%B8%D0%B8",
    image: "https://files.medelement.com/uploads/co/33581381815050/miniature/a7ffd0a89a32d9360568e1ad108236cd.png"
  },
  {
    id: 3,
    title: "Интубация и ИВЛ при COVID- 19(Аnesthesiology, март 2020)",
    resource: "medelement.com",
    url: "https://medelement.com/news/%D0%BA%D0%BE%D1%80%D0%BE%D0%BD%D0%B0%D0%B2%D0%B8%D1%80%D1%83%D1%81-covid-19-%D0%B4%D0%B8%D0%B0%D0%B3%D0%BD%D0%BE%D1%81%D1%82%D0%B8%D0%BA%D0%B0-%D0%B8-%D0%BB%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5/%D0%B8%D0%BD%D1%82%D1%83%D0%B1%D0%B0%D1%86%D0%B8%D1%8F-%D0%B8-%D0%B8%D0%B2%D0%BB-%D0%BF%D1%80%D0%B8-covid-19-anesthesiology-%D0%BC%D0%B0%D1%80%D1%82-2020",
    image: "https://files.medelement.com/uploads/co/33581381815050/miniature/d1f9b5245ca0e4c72caab226d3888d4b.jpg"
  },
]
