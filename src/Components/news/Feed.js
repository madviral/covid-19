import React, {useEffect, useLayoutEffect, useState} from 'react'
import { withRouter } from 'react-router-dom'
import List from './List'
import axios from "../../axiosConfig";
import { API } from "../../utils";

const NewsFeed = () => {
  const [news, setNews] = useState([])
  const [nextPage, setNextPage] = useState(1)
  const [numberOfPages, setNumberOfPages] = useState(1)
  const [isFetching, setIsFetching] = useState(false)
  const loadData = async (pageNum) => {
    const { data } = await axios({
      method: "GET",
      url: `${API}/v1/news?page=${pageNum}&pageSize=20`
    });

    setNews([...news, ...data.content]);
    setNumberOfPages(data.totalPages)
    setNextPage(data.pageNumber + 1)
    setIsFetching(false)
  }

  const handleScroll = () => {
    if (document.documentElement.scrollHeight - document.documentElement.scrollTop > document.documentElement.offsetHeight + 400) return;
    setIsFetching(true)
  }

  useLayoutEffect(() => {
    if (!isFetching) return;
    if (nextPage <= numberOfPages) {
      loadData(nextPage);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFetching]);

  useEffect(() => {
    loadData(1);
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <List news={news} />;
}

export default withRouter(NewsFeed)
