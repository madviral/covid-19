import React, {useEffect, useLayoutEffect, useState} from 'react'
import { withTranslation } from "react-i18next";
import clsx from 'clsx'
import axios from "../../axiosConfig";
import { getDateWithTime, API } from "../../utils";

const Leisure = ({ i18n }) => {
  const [list, setList] = useState([])
  const [nextPage, setNextPage] = useState(1)
  const [numberOfPages, setNumberOfPages] = useState(1)
  const [isFetching, setIsFetching] = useState(false)
  const loadData = async (pageNum) => {
    const { data } = await axios({
      method: "GET",
      url: `${API}/v1/leisure?page=${pageNum}&pageSize=20`
    });

    setList([...list, ...data.content]);
    setNumberOfPages(data.totalPages)
    setNextPage(data.pageNumber + 1)
    setIsFetching(false)
  }

  const handleScroll = () => {
    if (document.documentElement.scrollHeight - document.documentElement.scrollTop > document.documentElement.offsetHeight + 400) return;
    setIsFetching(true)
  }

  useLayoutEffect(() => {
    if (!isFetching) return;
    if (nextPage <= numberOfPages) {
      loadData(nextPage);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFetching]);

  useEffect(() => {
    loadData(1);
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {list.map(item => (
        <a
          href={item.url}
          className="news-item-link --leisure"
          rel="noopener noreferrer"
          target="_blank"
          key={item.id}
        >
          <section
            className={clsx("news-item", item.image && "--with-image")}
          >
            {item.image && (
              <div className="news-item__img-wrapper">
                <img
                  className="img-fluid"
                  src={item.image}
                  alt={item.title}
                />
              </div>
            )}
            <div className="news-item__content">
              <h4 className="news-item__title">{item.title}</h4>
              <time className="news-item__date">
                {getDateWithTime(item.created, i18n.language)}
              </time>
              <span className="news-item__resource">{item.resource}</span>
            </div>
          </section>
        </a>
      ))}
    </>
  );
}

export default withTranslation()(Leisure)
