import React, { useState } from 'react'
import clsx from 'clsx'

const Expander = ({ title, content, className }) => {
  const [opened, setOpened] = useState(false)

  return (
    <div className={clsx("expander", className, opened && "active")}>
      <div className="expander__header">
        <button
          className="expander__trigger"
          onClick={() => setOpened(!opened)}
        >
          {title}
        </button>
      </div>
      <div
        className="expander__content"
        dangerouslySetInnerHTML={{ __html: opened ? content : null }}
      />
    </div>
  );
}

export default Expander
