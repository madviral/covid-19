import React, { useEffect, useState } from "react"
import  { withRouter } from "react-router"
import {API} from "../../utils";
import axios from "../../axiosConfig";
import { LeisureMainLinks, LeisureSubcategory, DrawerHeader } from "./components";
import "./styles.css"

const LeisureCategory = ({ match, history }) => {
    const { params: { id } } = match;
    const [category, setCategory] = useState(null);
    const [categoryName, setCategoryName] = useState("")
    const [mainLinks, setMainLinks] = useState([]);
    const [subcategories, setSubcategories] = useState([]);
    const loadData = async id => {
        const { data } = await axios({
            method: "GET",
            url: `${API}/v1/leisure/sections/${id}/main`
        });
        setCategory(data);
    };

    useEffect(() => {
        if (category) {
            setMainLinks(category.sectionLeisureList.content);
            setSubcategories(category.categoryList.content);
            setCategoryName(category.leisureSectionDTO.name)
        }
    }, [category]);

    useEffect(() => {
        loadData(id)
    }, [id]);


    return (
        <section className={"leisure-page"}>
            <DrawerHeader onClick={() => history.goBack()} title={categoryName} />
            <LeisureMainLinks links={mainLinks} />
            {subcategories.map((sub, idx) =>
                <LeisureSubcategory
                    name={sub.name}
                    links={sub.leisurePage.content}
                    key={`leisure-subcategory-${idx}`}
                />)
            }
        </section>
    )
};

export default withRouter(LeisureCategory)
