import React, { useState, useEffect } from "react"
import { withTranslation } from "react-i18next";
import {API} from "../../utils";
import axios from "../../axiosConfig";
import { LeisureMainLinks, LeisureCategories } from "./components";
import "./styles.css"

const Leisure = ({ t }) => {
    const [mainLinks, setMainLinks] = useState([]);
    const [categories, setCategories] = useState([]);
    const loadData = async () => {
        const { data } = await axios({
            method: "GET",
            url: `${API}/v1/leisure/main`
        });

        setMainLinks(data.mainLeisureList.content);
        setCategories(data.sectionList.content)
    };

    useEffect(() => {
        loadData()
    }, []);

    return (<section className={"leisure-page"}>
        <div className="leisure-page__title-wrapper">
            <h1 className="leisure-page__title">{ `${t('leisure')} ${t('vLifeUpper')}` }</h1>
        </div>
        <LeisureMainLinks links={mainLinks} />
        <LeisureCategories
            categories={categories}
        />
    </section>)
};

export default withTranslation()(Leisure)
