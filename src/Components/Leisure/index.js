import React  from "react";
import { Route, Switch } from "react-router-dom";
import { Chat } from "../Chat";
import Leisure from "./Leisure";
import LeisureCategory from "./LeisureCategory";
import Rooms from "./Rooms";
import Questions from "./Questions";
import { RoomCreate } from "./components";

const Routes = () => (
  <Switch>
    <Route path="/leisure" exact component={Leisure} />

    <Route path="/leisure/category/:id" exact component={LeisureCategory} />

    <Route path="/leisure/rooms" exact component={Rooms} />

    <Route path="/leisure/rooms/create" exact component={RoomCreate} />

    <Route path="/leisure/chat/:id" exact component={Chat}/>

    <Route path="/leisure/questions" component={Questions} />
  </Switch>
)

export default Routes;
