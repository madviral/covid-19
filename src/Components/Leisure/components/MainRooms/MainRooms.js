import React from "react"
import MainRoom from "./MainRoom";
import "../styles.css"

const MainRooms = ({ rooms, onRoomClick }) => {
    return <div className="leisure-main-items">
        {rooms.map((room, index) =>
            <MainRoom
                onRoomClick={() => onRoomClick(room)}
                key={`main-room-${index}`}
                {...room}
            />)}
    </div>
};

MainRooms.defaultProps = {
    rooms: []
};

export default MainRooms
