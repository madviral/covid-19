import React from "react"
import "../styles.css"

const MainRoom = ({ img, title, onRoomClick }) => {

    return (
        <div className="leisure-main-item" onClick={onRoomClick}>
            <img className="leisure-main-item__img" src={img} alt={title}/>
            <p className="leisure-main-item__title">{title}</p>
        </div>
    )

};
export default MainRoom
