import React from "react"
import "./styles.css"
import Arrow from "../../../../icons/Arrow";

const DrawerHeader = ({ onClick, title }) => {
    return (
        <header className="drawer-header">
            <button className="drawer-header__arrow-button" onClick={onClick}>
                <Arrow />
            </button>
            <p className="drawer-header__title">{title}</p>
        </header>
    )
};

export default DrawerHeader
