import React from "react"
import clsx from "clsx"
import "../styles.css"
import "./styles.css"

const LeisureCategory = ({ imageUrl, name, onClick }) => {
    return <div onClick={onClick} className={clsx("leisure-two-columns-grid__item", "leisure-category")}>
        <div className="leisure-two-columns-grid__item-img-wrapper">
            <img className="leisure-two-columns-grid__item-img" src={imageUrl} alt={name}/>
        </div>
        <p className="leisure-two-columns-grid__item-title">{name}</p>
    </div>
};

export default LeisureCategory
