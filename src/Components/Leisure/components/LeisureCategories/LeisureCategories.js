import React from "react"
import { withRouter } from "react-router"
import { withTranslation } from "react-i18next";
import LeisureCategory from './LeisureCategory'
import "../styles.css"
import "./styles.css"

const LeisureCategories = ({ categories, t, history }) => {
    const redirectUrl = window.location.href
    return <div className="leisure-two-columns-grid">
        <LeisureCategory
            name={t('leisureRooms')}
            imageUrl="/images/room.jpg"
            onClick={() => history.push("/leisure/rooms")}
        />
        <LeisureCategory
           name={t('questionsAndAnswers')}
           imageUrl="/images/questions.jpg"
           onClick={() => history.push("/leisure/questions")}
        />
        <LeisureCategory
            name={t('brainWars')}
            imageUrl="/images/brain-wars.jpg"
            onClick={() => history.push("/brain-wars")}
        />
        <LeisureCategory
            name={'Покер'}
            imageUrl="/images/poker-bg.jpg"
            onClick={() => window.location = `https://poker-dev.vlife.kz/?redirectUrl=${redirectUrl}`}
        />
        {categories.map((category, index) =>
            <LeisureCategory
                onClick={() => history.push(`/leisure/category/${category.id}`)}
                key={`leisure-category-${index}`}
                {...category}
            />)
        }
    </div>
};

LeisureCategories.defaultProps = {
    categories: []
};

export default withTranslation()(withRouter(LeisureCategories))
