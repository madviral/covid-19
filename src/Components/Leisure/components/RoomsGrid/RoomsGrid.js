import React from "react"
import { withTranslation } from "react-i18next";
import { withRouter } from "react-router";
import clsx from "clsx"
import "../styles.css"
import "./styles.css"

const RoomsGrid = ({ rooms, t, onPlusButtonClick, onRoomClick, ...props }) => {
    const redirectUrl = window.location.href
    return (<div className="leisure-two-columns-grid">
        <div className={clsx("leisure-two-columns-grid__item", "rooms-grid__create-room-button-wrapper")}>
            <button className="rooms-grid__create-room-button" onClick={onPlusButtonClick}>+</button>
            <p className={clsx(
                "leisure-two-columns-grid__item-title",
                "rooms-grid__create-room-title"
                )}
            >
                {t('createNewRoom')}
            </p>
        </div>
        <a className="leisure-two-columns-grid__item" href={`https://poker-dev.vlife.kz/?redirectUrl=${redirectUrl}`}>
            <img className="leisure-two-columns-grid__item-img --poker" src='/images/poker-bg.jpg' width="155" height="120" alt='Покер' />
            <p className="leisure-two-columns-grid__item-title">Покер</p>
        </a>
        {rooms.map(room => (
            <div key={`room-${room.id}`} className="leisure-two-columns-grid__item" onClick={() => onRoomClick(room)}>
                <img className="leisure-two-columns-grid__item-img" src={room.img} alt={room.title}/>
                <p className="leisure-two-columns-grid__item-title">{room.title}</p>
            </div>
        ))}
    </div>)
};

RoomsGrid.defaultProps = {
    rooms: []
};

export default withTranslation()(withRouter(RoomsGrid))
