import React, { useState } from 'react'
import { withTranslation } from "react-i18next";
import { Link } from 'react-router-dom'
import axios from "../../../../axiosConfig";
import { API } from "../../../../utils";
import { useForm } from '../../../AlertReport/hooks'
import Arrow, { colors } from "../../../../icons/Arrow";
import './styles.css'

const Ask = ({ t, history }) => {
  const [isSended, setIsSended] = useState(false)
  const [form, setForm] = useForm({
    fio: '',
    subject: '',
    ownerEmail: ''
  })

  const handleSubmit = e => {
    e.preventDefault();
    const send = async () => {
      await axios({
        method: "POST",
        url: `${API}/v1/questions`,
        data: {
          ...form,
          body: '',
          categoryId: 1,
          ownerName: form.fio,
        }
      });
      setIsSended(true)
    }
    send()
  }

  return (
    <section className="leisure-page questions-page">
      <header className="drawer-header">
        <button className="drawer-header__arrow-button" onClick={history.goBack}>
          <Arrow color={colors.blue} />
        </button>
        <h2 className="drawer-header__title">
          {t('back')}
        </h2>
      </header>
      <div className="container">
        {
          isSended
            ? <>
                <p className="questions-page__text">Спасибо, за ваш вопрос. Через некоторое время он будет опубликован.</p>
                <Link className="btn questions-page__btn --center --blue-text --big-space" to='/leisure/questions'>
                  Перейти к вопросам
                </Link>
              </>
            : (
              <form className="answer-form" onSubmit={handleSubmit}>
                <legend>{t('questionsAndAnswers')}</legend>
                <textarea
                  id="subject"
                  type="text"
                  className="form-control"
                  name="subject"
                  autoFocus
                  value={form.subject}
                  onChange={setForm}
                  rows={1}
                  placeholder={t("askQuestion")}
                  required
                />
                <input
                  id="fio"
                  type="text"
                  className="form-control"
                  name="fio"
                  value={form.fio}
                  onChange={setForm}
                  placeholder={t("yourName")}
                  required
                />
                <input
                  id="ownerEmail"
                  type="email"
                  className="form-control"
                  name="ownerEmail"
                  value={form.ownerEmail}
                  onChange={setForm}
                  placeholder={t("yourEmail")}
                  required
                />
                <button className={`btn ${form.fio && form.subject && '--blue'}`}>
                  {t('askQuestion')}
                </button>
              </form>
            )
        }
       
      </div>
    </section>
  )
}

export default withTranslation()(Ask)
