import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { withTranslation } from "react-i18next";
import Header from './components/Header'
import axios from "../../../../axiosConfig";
import { API } from "../../../../utils";
import { useForm } from '../../../AlertReport/hooks'
import Arrow, { colors } from "../../../../icons/Arrow";
import './styles.css'

const Answer = ({ t, history, match, i18n }) => {
  const [data, setData] = useState(null)
  const [form, setForm] = useForm({
    fio: '',
    email: '',
    answer: '',
  })
  useEffect(() => {
    const load = async () => {
      const response = await axios({
        method: "GET",
        url: `${API}/v1/questions/${match.params.id}`
      })
      setData(response.data)
    }
    load()
  }, [])

  const handleSubmit = e => {
    e.preventDefault();
    const send = async () => {
      await axios({
        method: "POST",
        url: `${API}/v1/questions/${match.params.id}/answers`,
        data: {
          ownerEmail: form.email,
          ownerName: form.fio,
          text: form.answer,
        }
      });
      history.push(`/leisure/questions/q/${match.params.id}`)
    }
    send()
  }

  if (!data) {
    return null
  }

  return (
    <section className="leisure-page questions-page">
      <header className="drawer-header">
        <button className="drawer-header__arrow-button" onClick={history.goBack}>
          <Arrow color={colors.blue} />
        </button>
        <h2 className="drawer-header__title">
          {t('back')}
        </h2>
      </header>
      <div className="container">
        <section className="question">
          <div className="question__content">
            <Header name={data.ownerName} date={data.createdDate} locale={i18n.language} />
            <h4 className="question__subject --accent">{data.subject}</h4>
          </div>
        </section>
        
        <form className="answer-form" onSubmit={handleSubmit}>
          <legend>Ваш ответ</legend>
          <textarea
            id="answer"
            type="text"
            className="form-control"
            name="answer"
            autoFocus
            value={form.answer}
            onChange={setForm}
            rows={1}
            placeholder={t("yourAnswer")}
            required
          />
          <input
            id="email"
            type="email"
            className="form-control"
            name="email"
            value={form.email}
            onChange={setForm}
            placeholder={t("yourEmail")}
            required
          />
          <input
            id="fio"
            type="text"
            className="form-control"
            name="fio"
            value={form.fio}
            onChange={setForm}
            placeholder={t("yourName")}
            required
          />
          <button className={`btn ${form.fio && form.email && form.answer && '--blue'}`}>
            {t('answer')}
          </button>
        </form>
      </div>
    </section>
  )
}

export default withTranslation()(withRouter(Answer))
