import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { withTranslation } from "react-i18next";
import Header from './components/Header'
import axios from "../../../../axiosConfig";
import { API } from "../../../../utils";
import Arrow, { colors } from "../../../../icons/Arrow";
import './styles.css'

const Details = ({ t, history, match, i18n }) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    const load = async () => {
      const response = await axios({
        method: "GET",
        url: `${API}/v1/questions/${match.params.id}`
      })
      setData(response.data)
    }
    load()
  }, [])

  if (!data) {
    return null
  }

  return (
    <section className="leisure-page questions-page">
      <header className="drawer-header">
        <button className="drawer-header__arrow-button" onClick={() => history.push('/leisure/questions')}>
          <Arrow color={colors.blue} />
        </button>
        <h2 className="drawer-header__title">
          {t('back')}
        </h2>
      </header>
      <div className="container">
        <section className="question">
          <div className="question__content">
            <Header name={data.ownerName} date={data.createdDate} locale={i18n.language} />
            <h4 className="question__subject --accent">{data.subject}</h4>
          </div>
        </section>
        <Link className="btn --radius-xl --big-space questions-page__btn" to={`${match.url}/answer`}>
          {t('answerToQuestion')}
        </Link>
        <div className="questions-wrapper">
          {
            data.answers.length === 0 && <div className="question__empty">{t('haveNoAnswer')}</div>
          }
          {
            data.answers.map(a => (
              <section className="question" key={a.id}>
                <Header name={a.ownerName} date={a.createdDate} locale={i18n.language} />
                <h4 className="question__subject --list">{a.text}</h4>
              </section>
            ))
          }
        </div>
      </div>
    </section>
  )
}

export default withTranslation()(withRouter(Details))
