import React from 'react'
import { formatRelative, parseISO } from 'date-fns'
import { ru, kk } from 'date-fns/locale'

const Header = ({ name, date, locale }) => (
  <div className="question__header">
    <span className="question__author">{name}</span>
    &nbsp;/&nbsp;
    <span className="question__time">
      {
        formatRelative(date ? parseISO(date) : new Date(), new Date(), { locale: locale === 'ru' ? ru : kk })
      }
    </span>
  </div>
)

export default Header
