import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { withTranslation } from "react-i18next";
import Header from './components/Header'
import axios from "../../../../axiosConfig";
import { API } from "../../../../utils";
import Arrow, { colors } from "../../../../icons/Arrow";
import './styles.css'

const List = ({ t, history, match, i18n }) => {
  const [list, setList] = useState([])

  useEffect(() => {
    const load = async () => {
      const { data } = await axios({
        method: "GET",
        url: `${API}/v1/questions?catId=1&page=1&pageSize=20`
      })
      setList(data.content)
    }
    load()
  }, [])
  
  return (
    <section className="leisure-page questions-page">
      <header className="drawer-header">
        <button className="drawer-header__arrow-button" onClick={() => history.push('/leisure')}>
          <Arrow color={colors.blue} />
        </button>
        <h2 className="drawer-header__title">
          {t('questionsAndAnswers')}
        </h2>
      </header>
      <div className="container">
        <div className="disclaimer">
          <h4 className="disclaimer__title">
            Добро пожаловать в сервис Вопросов и Ответов!
          </h4>
          <p className="disclaimer__text">
            В этом разделе вы можете задать интересующий вас вопрос и получить на него ответ.
            <br />
            Обратите внимание, все вопросы проходят предварительную модерацию перед тем как будут опубликованы на сайте.
          </p>
          <strong>
            Запрещается:
          </strong>
          <ul className="disclaimer__list">
            <li>оскорблять участников и посетителей сай</li>
            <li>нарушать законы Республики Казахстан;</li>
          </ul>
        </div>
        
        <Link className="btn --with-icon --q-icon --radius-xl questions-page__btn" to={`${match.path}/ask`}>
          {t('askQuestion')}
        </Link>

        <div className="questions-wrapper">
          {
            list.map(question => (
              <section className="question --flex" key={question.id}>
                <div className="question__content">
                  <Header name={question.ownerName} date={question.createdDate} locale={i18n.language} />
                  <Link to={`${match.path}/q/${question.id}`}>
                    <h4 className="question__subject --list">{question.subject}</h4>
                  </Link>
                </div>
                <div className="question__btn">
                  <Link className="btn --mini --blue" to={`${match.path}/q/${question.id}/answer`}>
                    {t('answer')}
                  </Link>
                </div>
              </section>
            ))
          }
        </div>
      </div>
    </section>
  )
}

export default withTranslation()(withRouter(List))
