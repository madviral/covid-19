import React from "react"
import LeisureMainLink from './LeisureMainLink'
import "../styles.css"
import "./styles.css"

const LeisureMainLinks = ({ links }) => {
    return <div className="leisure-main-items">
            {links.map((link, index) => <LeisureMainLink key={`leisure-main-link-${index}`} {...link} />)}
        </div>
};

LeisureMainLinks.defaultProps = {
    links: []
};

export default LeisureMainLinks
