import React from "react"
import clsx from "clsx"
import { useImageContain } from '../../hooks/useImageContain'
import "../styles.css"
import "./styles.css"

const LeisureMainLink = ({ image, url, title, resource }) => {
  const isContain = useImageContain(image)
  return (
    <a className={clsx("leisure-main-item", "leisure-main-link")} href={url} target="_blank" rel="noopener noreferrer">
      <img
        className={`leisure-main-item__img ${isContain && '--contain'}`}
        src={image}
        alt={title}
      />
      <p className="leisure-main-item__title">{title}</p>
      <p className="leisure-main-item__resource">{resource}</p>
    </a>
  )
};
export default LeisureMainLink
