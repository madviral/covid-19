import React, { useState } from "react"
import { withRouter } from "react-router"
import { withTranslation } from "react-i18next";
import { SelectRooms, roomsOptions } from "../../../Select";
import axios from "../../../../axiosConfig";
import {API} from "../../../../utils";
import "../../styles.css"
import "./styles.css"

const RoomCreate = ({ t, history }) => {
    const [title, setTitle] = useState("");
    const [isPrivate, setIsPrivate] = useState(false);
    const [roomType, setRoomType] = useState(roomsOptions[0].value);
    const createChatRoom = async () => {
        const { data } = await axios({
            method: "POST",
            url: `${API}/chat/api/v1/room/create`,
            data: {
                title,
                isPrivate
            }
        });

        history.push(`/leisure/chat/${data.id}`);
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        switch (roomType) {
            case "CHAT":
                createChatRoom();
                break;
            case "POKER":
                console.log("poker");
                break;
            default:
                return
        }
    };

    return (
        <section className={"leisure-page"}>
            <header className="room-create-drawer__header">
                <p className="room-create-drawer__header-title">{t('newRoomCreation')}</p>
                <p onClick={() => history.goBack()} className="room-create-drawer__cancel">{t('cancel')}</p>
            </header>
            <form className="room-create-drawer__form" onSubmit={handleSubmit}>
                <div className="room-create-drawer__title-wrapper">
                    <input
                        className="room-create-drawer__title"
                        type="text"
                        id="room-create-drawer__title"
                        placeholder={t('inputRoomName')}
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                        required
                    />
                    <label
                        className="room-create-drawer__title-label"
                        htmlFor="room-create-drawer__title"
                    >
                        {t('roomName')}
                    </label>
                </div>
                <div className="room-create-drawer__radio-buttons-wrapper">
                    <label className="room-create-drawer__radio-button-wrapper">
                        <input
                            type="radio"
                            className="room-create-drawer__radio-button"
                            name="roomType"
                            checked={!isPrivate}
                            onChange={() => setIsPrivate(false)}
                        />
                        <div className="room-create-drawer__radio-check-wrapper">
                            <span className="room-create-drawer__radio-check-circle">
                                <span className="room-create-drawer__radio-check-inner-circle" />
                            </span>
                        </div>
                        {t('openRoom')}
                    </label>
                    <label className="room-create-drawer__radio-button-wrapper">
                        <input
                            type="radio"
                            className="room-create-drawer__radio-button"
                            name="roomType"
                            checked={isPrivate}
                            onChange={() => setIsPrivate(true)}
                        />
                        <div className="room-create-drawer__radio-check-wrapper">
                            <span className="room-create-drawer__radio-check-circle">
                                <span className="room-create-drawer__radio-check-inner-circle" />
                            </span>
                        </div>
                        {t('closedRoom')}
                    </label>
                </div>
                <SelectRooms onChange={setRoomType} value={roomType} className="room-create-drawer__select" />
                <button className="room-create-drawer__submit-button" type="submit">{t('createRoom')}</button>
            </form>
        </section>
    )
};

export default withTranslation()(withRouter(RoomCreate))
