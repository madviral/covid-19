import React from "react"

const LeisureSubcategoryItem = ({ link }) => {
  return (
    <a
      href={link.url}
      target="_blank"
      rel="noopener noreferrer"
      className="leisure-subcategory__link"
    >
      <img
        className='leisure-subcategory__link-img'
        src={link.image}
        alt={link.title}
      />
      <p className="leisure-subcategory__link-description">{link.title}</p>
    </a>
  )
}

export default LeisureSubcategoryItem
