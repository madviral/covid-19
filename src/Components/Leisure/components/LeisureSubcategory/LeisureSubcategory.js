import React from "react"
import { useImageContain } from '../../hooks/useImageContain'
import LeisureSubcategoryItem from './LeisureSubcategoryItem'
import "./styles.css"

const LeisureSubcategory = ({ name, links }) => {
  return <div className="leisure-subcategory">
    <h4 className={"leisure-subcategory__title"}>{name}</h4>
    <div className={"leisure-subcategory__links_grid"}>
      {
        links.map((link, idx) => (
          <LeisureSubcategoryItem
            key={`${name}-${idx}`}
            link={link}
          />
        ))
      }
    </div>
  </div>
};

LeisureSubcategory.defaultProps = {
    links: []
};

export default LeisureSubcategory
