import React from "react";
import { Route, Switch } from "react-router-dom";
import List from "./components/Questions/List"
import Details from "./components/Questions/Details"
import Answer from "./components/Questions/Answer"
import Ask from "./components/Questions/Ask"

const Questions = () => (
  <Switch>
    <Route
      path="/leisure/questions"
      exact
      component={List}
    />
    <Route
      path="/leisure/questions/q/:id"
      exact
      component={Details}
    />
    <Route
      path="/leisure/questions/q/:id/answer"
      exact
      component={Answer}
    />
    <Route
      path="/leisure/questions/ask"
      component={Ask}
    />
  </Switch>
)

export default Questions
