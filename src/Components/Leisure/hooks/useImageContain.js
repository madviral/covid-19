import { useEffect, useState } from "react"

export const useImageContain = imageUrl => {
  const [isContain, setIsContain] = useState(false)
  useEffect(() => {
    const proxyImg = new Image()
    proxyImg.src = imageUrl
    proxyImg.onload = function () {
      if (proxyImg.width > proxyImg.height) {
        setIsContain(true)
      }
    }
  }, [])

  return isContain
}
