import React, { useEffect, useState } from "react"
import  { withRouter } from "react-router"
import { withTranslation } from "react-i18next";
import axios from "../../axiosConfig";
import {API} from "../../utils";
import { DrawerHeader, RoomsGrid, MainRooms } from "./components";
import "./styles.css"

const Rooms = ({ t, history }) => {
    const [rooms, setRooms] = useState([]);
    const loadData = async () => {
        const { data } = await axios({
            method: "GET",
            url: `${API}/chat/api/v1/rooms`
        });

        setRooms(data)
    };

    useEffect(() => {
        loadData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onRoomClick = room => {
        history.push(`/leisure/chat/${room.id}`)
    };

    return (
        <section className={"leisure-page"}>
            <DrawerHeader onClick={() => history.push('/leisure')} title={t('leisureRooms')} />
            <MainRooms rooms={rooms.slice(-5)} onRoomClick={onRoomClick} />
            <RoomsGrid
                rooms={rooms}
                onPlusButtonClick={() => history.push("/leisure/rooms/create")}
                onRoomClick={onRoomClick}
            />
        </section>
    )
};

export default withTranslation()(withRouter(Rooms))
