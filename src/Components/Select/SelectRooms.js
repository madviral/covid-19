import React from 'react'
import i18next from "i18next";
import clsx from "clsx"
import Select from "react-select";
import { withTranslation } from "react-i18next";
import { DropdownIndicator } from "./SelectLocales";
import './styles.css'

export const roomsOptions = [
    { value: "CHAT", label: i18next.t('chat') },
    // { value: "POKER", label: i18next.t('poker') }
];

const SelectRooms = ({ value, onChange, t, className }) => {
    const [activeValue] = roomsOptions.filter(item => item.value === value)

    return (
        <Select
            components={{
                DropdownIndicator,
                IndicatorSeparator: null
            }}
            className={clsx("rooms-select", className)}
            classNamePrefix={"rooms-select"}
            value={activeValue}
            options={roomsOptions}
            onChange={({ value }) => onChange(value)}
            placeholder={t('chooseRoomType')}
            isSearchable={false}
            defaultValue={roomsOptions[0].value}
        />
    );
};

export default withTranslation()(SelectRooms)
