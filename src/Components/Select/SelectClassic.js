import React from 'react'
import Select, { components } from "react-select";
import './styles.css'

const DropdownIndicator = props => (
  <components.DropdownIndicator {...props}>
    <span className="icon-arrow --expander" />
  </components.DropdownIndicator>
);


const SelectClassic = ({ value, options, onChange }) => {
  const [activeValue] = options.filter(item => item.value === value)
  const baseName = 'select-classic'
  const className = activeValue?.label.length > 8 && activeValue?.label.length < 12
    ? `${baseName} --bigger`
    : activeValue?.label.length >= 12
      ? `${baseName} --more-bigger`
      : baseName
      
  return (
    <Select
      components={{
        DropdownIndicator,
        IndicatorSeparator: null
      }}
      className={className}
      classNamePrefix={baseName}
      value={activeValue}
      options={options}
      onChange={(value) => onChange(value)}
    />
  );
};

export default SelectClassic
