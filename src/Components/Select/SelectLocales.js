import React, { createElement } from 'react'
import clsx from 'clsx'
import Select, { components } from "react-select";
import './styles.css'

export const languageOptions = [
  { value: "kz", label: "ҚАЗ", icon: 'kz' },
  { value: "ru", label: "РУС", icon: 'ru' }
];

export const DropdownIndicator = props => (
  <components.DropdownIndicator {...props}>
    <span className="icon-arrow --down" />
  </components.DropdownIndicator>
);

const IndicatorSeparator = props => (
  <span className={clsx("languages-select__separator", `--${props.icon}`)} />
);

const SelectLocales = ({ value, options, onChange }) => {
  const [activeValue] = options.filter(item => item.value === value)

  return (
    <Select
      components={{
        DropdownIndicator,
        IndicatorSeparator: indicatorProps =>
          createElement(IndicatorSeparator, {
            ...indicatorProps,
            icon: activeValue?.icon
          })
      }}
      className={"languages-select"}
      classNamePrefix={"languages-select"}
      value={activeValue}
      options={options}
      onChange={({ value }) => onChange(value)}
    />
  );
};

export default SelectLocales
