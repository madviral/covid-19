import React from 'react'
import { withTranslation } from "react-i18next";
import WhatsAppIcon from '../icons/Wp'
import VkIcon from '../icons/Vk'
import TgIcon from '../icons/Tg'
import FbIcon from '../icons/Fb'

const Footer = ({ t }) => {
  const url = window.location.href
  const title = t('covid19Kz')
  return (
    <footer className="footer text-muted container">
      <div className="row ">
        <div className="col">
          <div className="social-share">
            {t('share')}:
            <a
              className='social-share__link'
              data-action="share/whatsapp/share"
              href={`https://api.whatsapp.com/send?text=${url}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              <WhatsAppIcon />
            </a>
            <a
              className='social-share__link'
              href={`https://telegram.me/share/url?url=${url}&text=${title}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              <TgIcon />
            </a>
            <a
              className='social-share__link'
              href={`https://www.facebook.com/sharer/sharer.php?u=${url}&t=${title}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              <FbIcon />
            </a>
            <a
              className='social-share__link'
              href={`http://vk.com/share.php?url=${url}&title=${title}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              <VkIcon />
            </a>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default withTranslation()(Footer)
