/* eslint-disable jsx-a11y/iframe-has-title */
import React from 'react'

const Graphs = ({ time }) => (
  <div className='page'>
    <div className="container">
      <div className="date-wrapper">
        <div className="date-title">{time}</div>
      </div>
    </div>
    <iframe 
      width={window.innerWidth}
      height={window.innerHeight - 32}
      src='https://app.powerbi.com/view?r=eyJrIjoiN2Y0NjNlOTUtYmZiZS00MmE0LWFiMmEtZDBjNzJiYmI5YjQ1IiwidCI6IjFkMjg2M2NlLWI3ZTUtNGMyNi05MDYwLWVkOGNjMTE1OWUxMiIsImMiOjl9'
      frameBorder={0}
      allowFullScreen
    />
  </div>
)

export default Graphs
