import React, { Component } from "react";
import i18n from "i18next";
import cookie from "js-cookie";
import axios from "../axiosConfig";
import { withTranslation } from "react-i18next";
import DG from "2gis-maps";
import Statistics from "./Statistics";
import { API } from "../utils";
import kz from "../utils/data/kz.json";
import { SelectLocales, Select } from "./Select";
import { languageOptions } from "./Select/SelectLocales";
import SpriteMap from './SpriteMap'
import MapDrawer from "./MapDrawer/MapDrawer";
import "./main.css";

const DEFAULT_VIEWPORT = {
  center: [48.0196, 66.9237],
  zoom: 4.5
};
const getRadius = infected => {
  if (infected < 1000) {
    const kef = 4 * Math.min(Math.floor((infected - 1) / 12), 6);
    return [24 + kef, 24 + kef];
  }

  return [96, 96];
};

let MAP;

const createClusterCustomIcon = cluster => {
  const children = cluster.getAllChildMarkers();
  const countTitles = children.map(item => item.options.title);
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const customCount = countTitles.reduce(reducer);
  return DG.divIcon({
    html: `<span class="marker-cluster-number">${customCount}</span>`,
    className: "marker-cluster-custom",
    iconSize: DG.point(40, 40, true)
  });
};

export class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 20,
      lng: -15.301048,
      countries: [],
      general: [],
      cities: [],
      places: [],
      updated: "",
      viewport: DEFAULT_VIEWPORT,
      screenWidth: null,
      mapDrawerIsOpen: false,
      mapZoom: 9
    };
    this.popup = React.createRef();
  }

  sendToNativeIos(message) {
    if (
      window &&
      window.webkit &&
      window.webkit.messageHandlers &&
      window.webkit.messageHandlers.nativeProcess
    ) {
      const native = window.webkit.messageHandlers.nativeProcess;
      native.postMessage(message);
    }
  }

  sendToNativeAndroid(message) {
    // eslint-disable-next-line no-undef
    if (typeof Android !== "undefined") {
      // eslint-disable-next-line no-undef
      Android.sendData(message);
    }
  }

  changeLang = lang => {
    i18n.changeLanguage(lang);
    cookie.set("next-i18next", lang);
    this.sendToNativeIos(lang);
    this.sendToNativeAndroid(lang);
    this.setMap();
  };

  componentDidMount() {
    this.setMap();
  }

  setMap() {
    const isMobile = window.innerWidth <= 640;
    axios({
      method: "GET",
      url: `${API}/v1/status`
    })
      .then(({ data }) => {
        this.setState({
          total: data.country.infected,
          recovered: data.country.recovered,
          deaths: data.country.deaths,
          places: data.places,
          currentCity: data.cities.find(item => item.id === 1),
          cities: data.cities.sort((a, b) => b.infected - a.infected),
          updated: data.country.updated,
          isMobile
        });
        return { places: data.places, currentCity: this.state.currentCity };
      })
      .then(({ places, currentCity }) => {
        DG.then(function () {
          return DG.plugin(
            "https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"
          );
        })
          .then(function () {
              MAP = DG.map("map", {
                center: isMobile ? [currentCity.latitude, currentCity.longitude] : DEFAULT_VIEWPORT.center,
                zoom: isMobile ? 9 : 5,
                fullscreenControl: !isMobile,
                zoomControl: !isMobile,
              });
            
            DG.geoJson(kz, {
              fillColor: "transparent",
              weight: 1,
              opacity: 1,
              color: "#2F80ED",
              fillOpacity: 0.3
            }).addTo(MAP);

            const markers = DG.markerClusterGroup({
              iconCreateFunction: createClusterCustomIcon,
              maxClusterRadius: 40
            });
            for (let i = 0; i < places.length; i++) {
              const a = places[i];
              const markerIcon = DG.divIcon({
                className: "my-custom-icon",
                iconSize: getRadius(a.infected),

                html: `<span class="icon-number">${a.infected}</span>`
              });
              const marker = DG.marker([a.latitude, a.longitude], {
                icon: markerIcon,
                title: a.infected
              });

              if (a.infected > 0) {
                markers.addLayer(marker);
              }
            }

            MAP.addLayer(markers);
            return MAP;
          })
          .then(MAP => {
            axios({
              method: "GET",
              url: `${API}/v1/roadblocks`
            }).then(({ data }) => {
              const markers = DG.markerClusterGroup({
                iconCreateFunction: () =>
                  DG.divIcon({
                    className: "marker-roadblock-cluster-custom",
                    iconSize: DG.point(0, 0, true)
                  }),
                maxClusterRadius: 12
              });
              for (let i = 0; i < data.length; i++) {
                const a = data[i];
                const markerIcon = DG.divIcon({
                  className: "roadblock-icon",
                  iconSize: 20,
                  html: a.name
                });
                const marker = DG.marker([a.latitude, a.longitude], {
                  icon: markerIcon
                });
                marker.bindPopup(`<b>Блокпост: ${a.name}</b>`, {
                  sprawling: true
                });
                markers.addLayer(marker);
              }
              MAP.addLayer(markers);
            });
          });
      });
  }

  changeCity(value) {
    const currentCity = this.state.cities.find(
      item => item.id === Number(value)
    );
    this.setState({ currentCity }, () => {
      MAP.setView([currentCity.latitude, currentCity.longitude]);
    });
  }

  handleClickSTaticMap = () => {
    this.setState({
      mapDrawerIsOpen: true,
      drawerCity: this.state.cities[0],
      mapZoom: 4,
    })
  }

  toggleMapDrawerIsOpen() {
    if (this.state.isMobile) {
      this.setState({
        mapDrawerIsOpen: !this.state.mapDrawerIsOpen,
        drawerCity: this.state.currentCity,
        mapZoom: 9
      })
    }
  }
  render() {
    const { t, i18n } = this.props;
    const { cities, deaths, isMobile, recovered, total } = this.state
    return (
      <div className="main-page">
        <div className="wrapper mobile-only ">
          <div className="title-wrapper">
            <h2 className="page-title">
              <b>{t("covid19Kz")}</b>
              <b>{t("victory")}</b>
            </h2>
            <SelectLocales
              value={i18n.language}
              onChange={this.changeLang}
              options={languageOptions}
            />
          </div>
          <div className="page_statistics">
            <div className="statistic --red">
              <div className="stat__title">{t("detected")}</div>
              <div className="stat__count">{total}</div>
            </div>
            <div className="statistic --green">
              <div className="stat__title">{t("healed")}</div>
              <div className="stat__count">{recovered}</div>
            </div>
            <div className="statistic --gray">
              <div className="stat__title">{t("dead")}</div>
              <div className="stat__count">{deaths}</div>
            </div>
          </div>
        </div>
        <div className="mobile-only">
          <SpriteMap mobile={isMobile} cities={cities} onClick={this.handleClickSTaticMap} />
        </div>
        <div className="wrapper mobile-only ">
          <div className="city__settings">
            <div className="city__settings-label">{t('yourCity')}</div>
            <Select
              value={this.state.currentCity?.id}
              onChange={value => this.changeCity(value.id)}
              options={this.state.cities.map(city => ({ ...city, value: city.id, label: city.name }))}
            />
          </div>
        </div>

        <div className="map-wrapper loop">
          <div id="map" onClick={() => this.toggleMapDrawerIsOpen()} className="map" />
        </div>
        <div className="col page-cards">
          <br />
          <div className="bd-example">
            <div className="card card-body text-center card-wrapper">
              <div
                className="card  card-block "
                style={{
                  width: "18rem",
                  color: "#eb5757",
                  borderColor: "#eb5757",
                  marginBottom: 10
                }}
              >
                <div className="card-body">
                  <h2 className="card-title">{this.state.total}</h2>
                  <p className="card-text">{t("detected")}</p>
                </div>
              </div>

              <div
                className="card border-success "
                style={{ width: "18rem", marginBottom: 10 }}
              >
                <div className="card-body text-success">
                  <h2 className="card-title">{this.state.recovered}</h2>
                  <p className="card-text">{t("healed")}</p>
                </div>
              </div>

              <div
                className="card"
                style={{
                  width: "18rem",
                  color: "#828282",
                  borderColor: "#828282",
                  marginBottom: 10
                }}
              >
                <div
                  className="card-body "
                  style={{
                    color: "#828282",
                    borderColor: "#828282"
                  }}
                >
                  <h2 className="card-title">{this.state.deaths}</h2>
                  <p className="card-text">{t("dead")}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col page-statistic">
          <div className="container">
            <Statistics
              cities={this.state.cities}
              updated={this.state.updated}
            />
          </div>
        </div>
        {
          isMobile && <MapDrawer
            handleToggleDrawer={() => this.toggleMapDrawerIsOpen()}
            currentCity={this.state.drawerCity}
            statistic={{ total, recovered, deaths }}
            recovered={recovered}
            places={this.state.places}
            isOpen={this.state.mapDrawerIsOpen}
            getRadius={getRadius}
            mapZoom={this.state.mapZoom}
            createClusterCustomIcon={createClusterCustomIcon}
          />
        }
      </div>
    );
  }
}

export default withTranslation()(Main);
