import React from 'react'
import { withTranslation } from "react-i18next";
import { useForm } from "./hooks";

const WithPlacesTags = ({ list, title, onAdd, t }) => {
  const [form, setForm, , , resetAll] = useForm({
    place: "",
  });
  const submitHandler = e => {
    e.preventDefault();
    if (form.place) {
      onAdd([...list, form]);
      resetAll();
    }
  };
  const onRemoveTag = place => {
    const filtered = list.filter(item => item.place !== place);
    onAdd(filtered);
  };
  return (
    <div className="with-tags">
      <h4 className="form-control-title">{title}</h4>
      <div className="with-tags__wrapper">
        {list.map(item => (
          <div className="with-tags__tag" key={Math.random() * 1000}>
            <span className="tag">
              {item.place}
              <button
                className="tag__remove close-btn"
                onClick={() => onRemoveTag(item.place)}
              >
                x
              </button>
            </span>
          </div>
        ))}
      </div>
      <div className="input-group report-inner__input">
        <div className="input-group report-inner__input">
          <input
            id={`place-${Date.now()}`}
            type="text"
            className="form-control"
            name="place"
            value={form.place}
            onChange={setForm}
            placeholder={t("placeWhereWasInfection")}
          />
        </div>
        <button className="with-tags__add-btn" onClick={submitHandler}>
          +
        </button>
      </div>
    </div>
  );
};

export default withTranslation()(WithPlacesTags);
