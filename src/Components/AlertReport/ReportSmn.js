import React from 'react'
import { withTranslation } from "react-i18next";
import {
  ModalProvider,
  ModalConsumer,
  ModalOpener,
  ModalRoot
} from "./modal";
import ReportSmnModal from "./ReportSmnModal";

const ReportSmn = ({ hideParentModal, t }) => {
  return (
    <ModalProvider>
      <ModalRoot />
      <ModalConsumer>
        {({ showModal, hideModal }) => (
          <ModalOpener
            showModal={showModal}
            trigger={
              <button
                className="btn report__modal-btn --white --center"
                data-toggle="modal"
                data-target="#reportModal"
              >
                  { t('iHaveContactedWithInfected') }
              </button>
            }
          >
            <ReportSmnModal
              showModal={showModal}
              onHide={hideModal}
              hideParentModal={hideParentModal}
            />
          </ModalOpener>
        )}
      </ModalConsumer>
    </ModalProvider>
  );
}

export default withTranslation()(ReportSmn);
