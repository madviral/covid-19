import React from 'react'
import { withTranslation } from "react-i18next";
import InputPhone from '../input/InputPhone'
import { useForm } from "./hooks";

const WithTags = ({ list, title, onAdd, t }) => {
  const [form, setForm, , , resetAll] = useForm({
    fio: "",
    number: ""
  });
  const submitHandler = e => {
    e.preventDefault();
    if (form.fio) {
      onAdd([...list, form]);
      resetAll();
    }
  }
  const onRemoveTag = fio => {
    const filtered = list.filter(item => item.fio !== fio);
    onAdd(filtered)
  }
  return (
    <div className="with-tags">
      <h4 className="form-control-title">{title}</h4>
      <div className="with-tags__wrapper">
        {list.map(item => (
          <div className="with-tags__tag" key={Math.random() * 1000}>
            <span className="tag">
              {item.fio}
              <button
                className="tag__remove close-btn"
                onClick={() => onRemoveTag(item.fio)}
              >
                x
              </button>
            </span>
          </div>
        ))}
      </div>
      <div className="input-group report-inner__input">
        <input
          id={`infectedPersonFio-${Date.now()}`}
          type="text"
          className="form-control"
          name="fio"
          value={form.fio}
          onChange={setForm}
          placeholder={t("firstAndSecondName")}
        />
        <InputPhone
          id="number"
          placeholder={t("mobilePhone")}
          value={form.number}
          onChange={setForm}
        />
        <button className="with-tags__add-btn" onClick={submitHandler}>
          +
        </button>
      </div>
    </div>
  );
};

export default withTranslation()(WithTags);
