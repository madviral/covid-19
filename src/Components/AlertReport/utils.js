export const PLANE = "Авиатранспорт";
export const TRAIN = "ЖД";
export const AUTO = "Автотранспорт";

export const getVehicleType = ({
  vehicleTypePlane,
  vehicleTypeTrain,
  vehicleTypeAuto,
  vehicleTypePlaneNumber,
  vehicleTypePlaneRoute,
  vehicleTypeTrainNumber,
  vehicleTypeTrainRoute,
  vehicleTypeAutoNumber,
  vehicleTypeAutoRoute
}) => {
  const vehicleType = [];
  if (vehicleTypePlane) {
    vehicleType.push({
      name: vehicleTypePlane,
      number: vehicleTypePlaneNumber,
      route: vehicleTypePlaneRoute
    });
  }
  if (vehicleTypeTrain) {
    vehicleType.push({
      name: vehicleTypeTrain,
      number: vehicleTypeTrainNumber,
      route: vehicleTypeTrainRoute
    });
  }
  if (vehicleTypeAuto) {
    vehicleType.push({
      name: vehicleTypeAuto,
      number: vehicleTypeAutoNumber,
      route: vehicleTypeAutoRoute
    });
  }
  return JSON.stringify(vehicleType);
}
