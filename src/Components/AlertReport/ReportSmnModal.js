import React from "react";
import WithTags from "./WithTags";
import WithPlacesTags from "./WithPlacesTags";
import axios from "../../axiosConfig";
import ReportSuccess from "./ReportSuccess";
import { withTranslation } from "react-i18next";
import { AlertModal } from "./modal";
import InputPhone from "../input/InputPhone";
import { useForm } from "./hooks";
import { getVehicleType, PLANE, TRAIN, AUTO } from "./utils";
import { API } from "../../utils";
const ReportSmnModal = ({ showModal, onHide, hideParentModal, t }) => {
  const [form, setForm, setCbx, setValue] = useForm({
    fio: "",
    cellPhone: "",
    homePhone: "",
    address: "",
    comeFrom: "",
    vehicleTypePlane: "",
    vehicleTypeTrain: "",
    vehicleTypeAuto: "",
    vehicleTypePlaneNumber: "",
    vehicleTypePlaneRoute: "",
    vehicleTypeTrainNumber: "",
    vehicleTypeTrainRoute: "",
    vehicleTypeAutoNumber: "",
    vehicleTypeAutoRoute: "",
    infectedPersonFio: "",
    infectedPersonPhone: "",
    infectedPlace: [],
    contactPersons: [],
    visitPlaces: [],
    name: ""
  });

  const handleSubmit = e => {
    e.preventDefault();
    const send = async () => {
      const vehicleType = getVehicleType(form);
      await axios({
        method: "POST",
        url: `${API}/v1/reports/contacts`,
        data: {
          ...form,
          vehicleType,
          infectedPerson: form.infectedPersonFio
            ? JSON.stringify({
                fio: form.infectedPersonFio,
                phone: form.infectedPersonPhone
              })
            : "",
          infectedPlace: JSON.stringify(form.infectedPlace),
          contactPersons: JSON.stringify(form.contactPersons),
          visitPlaces: JSON.stringify(form.visitPlaces)
        }
      });
      showModal(
        <ReportSuccess onHide={onHide} hideParentModal={hideParentModal} />
      );
    };
    send();
  };

  return (
    <AlertModal id="reportModal" role="dialog" isWhite onClose={onHide}>
      <div className="report-inner">
        <h3 className="report-inner__title">
          {t("iHaveContactedWithInfected")}
        </h3>
        <p className="report-inner__hint">
          {t("pleaseTellAboutYourCondition")}!
        </p>
        <form onSubmit={handleSubmit}>
          <div className="input-group report-inner__input">
            <input
              id="fio"
              autoFocus
              type="text"
              className="form-control"
              name="fio"
              value={form.fio}
              onChange={setForm}
              placeholder={t("firstAndSecondName")}
              required
            />
          </div>
          <div className="input-group report-inner__input">
            <InputPhone
              id="cellPhone"
              placeholder={t("mobilePhone")}
              value={form.cellPhone}
              onChange={setForm}
              required
            />
          </div>
          <div className="input-group report-inner__input">
            <InputPhone
              id="homePhone"
              placeholder={t("homePhone")}
              value={form.homePhone}
              mask="+7 (999) 999 99 99"
              onChange={setForm}
            />
          </div>
          <div className="input-group report-inner__input">
            <input
              id="address"
              type="text"
              className="form-control"
              name="address"
              placeholder={t("address")}
              value={form.address}
              onChange={setForm}
              required
            />
          </div>
          <div className="with-tags">
            <h4 className="form-control-title">
              {t("suggestedInfectedPerson")}
            </h4>
            <div className="input-group report-inner__input">
              <input
                id="infectedPersonFio"
                type="text"
                className="form-control"
                name="infectedPersonFio"
                value={form.infectedPersonFio}
                onChange={setForm}
                placeholder={t("firstAndSecondName")}
              />
              <InputPhone
                id="infectedPersonPhone"
                placeholder={t("mobilePhone")}
                value={form.infectedPersonPhone}
                onChange={setForm}
              />
            </div>
          </div>
          <WithPlacesTags
            list={form.infectedPlace}
            title={t("placeWhereWasInfection")}
            onAdd={value => setValue("infectedPlace", value)}
          />
          <div className="vehicle-controls report-inner__input">
            <h4 className="vehicle-controls__title">
              {t("transportTypeYouTraveled")}
            </h4>
            <div className="vahicle-controls__content">
              <div className="vehicle-controls__item">
                <label>
                  <input
                    type="checkbox"
                    id="plane"
                    name="vehicleTypePlane"
                    value={PLANE}
                    checked={form.vehicleTypePlane === PLANE}
                    onChange={setCbx}
                  />
                  <span>{t("avia")}</span>
                  <i className="vehicle-controls__item-icon icon --plane" />
                </label>
                {form.vehicleTypePlane === PLANE && (
                  <div className="vehicle-controls__item-input">
                    <input
                      id="vehicleTypePlaneNumber"
                      type="text"
                      className="form-control"
                      name="vehicleTypePlaneNumber"
                      value={form.vehicleTypePlaneNumber}
                      onChange={setForm}
                      placeholder={t("planeNumber")}
                    />
                    <input
                      id="vehicleTypePlaneRoute"
                      type="text"
                      className="form-control"
                      name="vehicleTypePlaneRoute"
                      value={form.vehicleTypePlaneRoute}
                      onChange={setForm}
                      placeholder={t("route")}
                    />
                  </div>
                )}
              </div>
              <div className="vehicle-controls__item">
                <label>
                  <input
                    type="checkbox"
                    id="train"
                    name="vehicleTypeTrain"
                    value={TRAIN}
                    checked={form.vehicleTypeTrain === TRAIN}
                    onChange={setCbx}
                  />
                  <span>{t("rail")}</span>
                  <i className="vehicle-controls__item-icon icon --train" />
                </label>
                {form.vehicleTypeTrain === TRAIN && (
                  <div className="vehicle-controls__item-input">
                    <input
                      id="vehicleTypeTrainNumber"
                      type="text"
                      className="form-control"
                      name="vehicleTypeTrainNumber"
                      value={form.vehicleTypeTrainNumber}
                      onChange={setForm}
                      placeholder={t("trainNumber")}
                    />
                    <input
                      id="vehicleTypeTrainRoute"
                      type="text"
                      className="form-control"
                      name="vehicleTypeTrainRoute"
                      value={form.vehicleTypeTrainRoute}
                      onChange={setForm}
                      placeholder={t("route")}
                    />
                  </div>
                )}
              </div>
              <div className="vehicle-controls__item">
                <label>
                  <input
                    type="checkbox"
                    id="auto"
                    name="vehicleTypeAuto"
                    value={AUTO}
                    checked={form.vehicleTypeAuto === AUTO}
                    onChange={setCbx}
                  />
                  <span>{t("auto")}</span>
                  <i className="vehicle-controls__item-icon icon --auto" />
                </label>
                {form.vehicleTypeAuto === AUTO && (
                  <div className="vehicle-controls__item-input">
                    <input
                      id="vehicleTypeAutoNumber"
                      type="text"
                      className="form-control"
                      name="vehicleTypeAutoNumber"
                      value={form.vehicleTypeAutoNumber}
                      onChange={setForm}
                      placeholder={t("autoNumber")}
                    />
                    <input
                      id="vehicleTypeAutoRoute"
                      type="text"
                      className="form-control"
                      name="vehicleTypeAutoRoute"
                      value={form.vehicleTypeAutoRoute}
                      onChange={setForm}
                      placeholder={t("route")}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
          <WithTags
            list={form.contactPersons}
            title={t("personYourContactedWith")}
            onAdd={value => setValue("contactPersons", value)}
          />
          <WithPlacesTags
            list={form.visitPlaces}
            title={t("placesYouVisited")}
            onAdd={value => setValue("visitPlaces", value)}
          />
          <button className="btn --blue report-inner__btn">
            {t("report")}
          </button>
        </form>
      </div>
    </AlertModal>
  );
};

export default withTranslation()(ReportSmnModal);
