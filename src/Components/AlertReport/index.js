import React from 'react'
import { withTranslation } from "react-i18next";
import { withRouter } from "react-router-dom";
import clsx from 'clsx';
import {
  AlertModal,
  ModalProvider,
  ModalConsumer,
  ModalOpener,
  ModalRoot
} from "./modal";
import ReportOwn from './ReportOwn'
import ReportSmn from './ReportSmn'
import './styles.css'

const AlertReport = ({ location, t, i18n }) => {
  const query = new URLSearchParams(location.search)
  if (query.get("source") === "ios" || query.get("source") === "android") {
    return null;
  }

  return (
    <div className="report">
      <ModalProvider>
        <ModalRoot />
        <ModalConsumer>
          {({ showModal, hideModal }) => (
            <ModalOpener
              showModal={showModal}
              trigger={
                <button
                  className={clsx(
                    "btn",
                    "report__btn",
                    "--alert",
                    "--radius-xl",
                    i18n.language !== 'ru' && '--mini'
                  )}
                  data-toggle="modal"
                  data-target="#reportModal"
                  onClick={showModal}
                >
                  {t("reportCase")}
                </button>
              }
            >
              <AlertModal id="reportModal" role="dialog">
                <p className="report__hint">
                  {t("pleaseTellAboutYourCondition")}!
                </p>
                <div className="report__modal-content">
                  <ReportSmn hideParentModal={hideModal} />
                  <ReportOwn hideParentModal={hideModal} />
                </div>
              </AlertModal>
            </ModalOpener>
          )}
        </ModalConsumer>
      </ModalProvider>
    </div>
  );
};

export default withTranslation()(withRouter(AlertReport));
