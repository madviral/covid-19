import React from 'react'
import { withTranslation } from "react-i18next";
import {
  ModalProvider,
  ModalConsumer,
  ModalOpener,
  ModalRoot
} from "./modal";
import ReportOwnModal from "./ReportOwnModal";

const ReportOwn = ({ hideParentModal, t }) => {
  return (
    <ModalProvider>
      <ModalRoot />
      <ModalConsumer>
        {({ showModal, hideModal }) => (
          <ModalOpener
            showModal={showModal}
            trigger={
              <button
                className="btn report__modal-btn --white --center"
                data-toggle="modal"
                data-target="#reportModal"
              >
                  { t('iAmInfected') }
              </button>
            }
          >
            <ReportOwnModal
              showModal={showModal}
              onHide={hideModal}
              hideParentModal={hideParentModal}
            />
          </ModalOpener>
        )}
      </ModalConsumer>
    </ModalProvider>
  );
}

export default withTranslation()(ReportOwn);
