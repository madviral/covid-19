import { useState } from "react";

export const useForm = initialValues => {
  const [values, setValues] = useState(initialValues);
  const setCbx = e => {
    setValues({
      ...values,
      [e.target.name]: values[e.target.name] ? '': e.target.value,
    });
  }
  const resetAll = () => setValues(initialValues)
  const setValue = (field, value) =>
    setValues({
      ...values,
      [field]: value
    });
  return [
    values,
    e => {
      setValues({
        ...values,
        [e.target.name]: e.target.value
      });
    },
    setCbx,
    setValue,
    resetAll
  ];
};
