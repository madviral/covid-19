import React from 'react'
import { withTranslation } from "react-i18next";
import { BasicModal } from "./modal";

const ReportSuccess = ({ onHide, hideParentModal, t }) => {
  const handleClose = () => {
    onHide();
    hideParentModal();
  }
  return (
    <BasicModal
      id="reportModal"
      role="dialog"
      width="100%"
      height="100vh"
      onClose={handleClose}
    >
      <div className="report-response">
        <i className="report-response__icon icon --checkmark" />
        <div className="report-response__title">{t("sended")}</div>
        <p className="report-response__descr">
          {t("reportSuccessDescription")}
        </p>
        <button class="btn report-response__btn" onClick={handleClose}>
          {t("close")}
        </button>
      </div>
    </BasicModal>
  );
};

export default withTranslation()(ReportSuccess);