import React from 'react'
import { withTranslation } from "react-i18next";
import Body from './Body'
import Background from './Background'
import Modal from './Modal'
import { useOpenedAndOffsetClose } from './hooks'
import './styles.css'
import { SLIDE_Y } from './utils'
import Button from './Button'
import clsx from 'clsx'

const AlertModal = ({ children, isWhite, onClose, onLoad, t }) => {
  const duration = 150
  const { opened, handleClose } = useOpenedAndOffsetClose(duration, onClose)

  return (
    <Modal className={clsx("--flex-end", !isWhite && "--red")} onLoad={onLoad}>
      <Background duration={duration} opened={opened} />
      <Body
        animeType={SLIDE_Y}
        duration={duration}
        className="modal__body"
        opened={opened}
      >
        {typeof children === "function" ? children({ handleClose }) : children}
        <Button
          className={clsx(
            !isWhite ? "modal__cancel" : "modal__close",
            "close-btn"
          )}
          onClick={handleClose}
        >
          { t('cancel') }
        </Button>
      </Body>
    </Modal>
  );
}

AlertModal.defaultProps = {}

export default withTranslation()(AlertModal)
