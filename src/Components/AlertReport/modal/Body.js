import React from 'react'
import clsx from 'clsx'
import { animated, interpolate } from "react-spring";
import { getStyles, POP } from "./utils";

const Body = ({
  animeType,
  className,
  duration,
  opened,
  children,
  styles,
}) => {
  const getStyleProps = getStyles(animeType, { duration, opened });
  const styleProps = getStyleProps()

  return (
    <animated.div
      style={{
        ...styles,
        ...styleProps,
        ...(styleProps.scale
          ? {
              transform: interpolate(
                [styleProps.scale],
                s => `scale(${s})`,
              ),
            }
          : {}),
      }}
      className={clsx(className)}
    >
      {children}
    </animated.div>
  )
}

Body.defaultProps = {
  animeType: POP,
  duration: 150,
  opened: false
};

export default Body
