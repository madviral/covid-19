export { default as BaseModal } from './Modal'
export { default as BasicModal } from "./BasicModal";
export { default as AlertModal } from './AlertModal'

export {
  ModalContext,
  ModalConsumer,
  ModalOpener,
  ModalProvider,
  ModalRoot
} from "./Context";
