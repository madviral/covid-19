import React from 'react'
import Background from './Background'
import Body from './Body'
import Modal from './Modal'
import { useOpenedAndOffsetClose } from './hooks'

const BasicModal = ({ width, height, children, onClose, onLoad }) => {
  const duration = 150
  const { opened, handleClose } = useOpenedAndOffsetClose(duration, onClose)

  return (
    <Modal onLoad={onLoad}>
      <Background duration={duration} opened={opened} />
      <Body
        className="modal__body"
        styles={{ width, height }}
        duration={duration}
        opened={opened}
      >
        {typeof children === "function" ? children({ handleClose }) : children}
        {onClose && (
          <button className="close-btn modal__close" onClick={handleClose}>
            x
          </button>
        )}
      </Body>
    </Modal>
  );
}

BasicModal.defaultProps = {
  width: "auto",
  height: "auto"
};

export default BasicModal
