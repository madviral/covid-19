import { useEffect, useState } from 'react'

export const useOpenedAndOffsetClose = (
  duration,
  onClose,
) => {
  const [opened, setOpened] = useState(false)
  useEffect(() => {
    setOpened(true)

    return () => setOpened(false);  
  }, [])

  const handleClose = () => {
    setOpened(false)

    if (onClose) {
      setTimeout(() => onClose(), duration)
    }
  }

  return { opened, handleClose }
}

// TODO: move to solid file and use https://github.com/willmcpo/body-scroll-lock instead simple body scroll
const disableHandler = e => {
  e.preventDefault()
}

export const useBodyLock = () => {
  useEffect(() => {
    document.body.addEventListener('touchmove', disableHandler, {
      passive: false,
    })

    return () => {
      document.body.removeEventListener('touchmove', disableHandler)
    }
  })
}
