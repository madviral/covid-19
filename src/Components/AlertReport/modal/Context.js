import React, {
  cloneElement,
  createContext,
  useState,
} from 'react'

export const ModalContext = createContext({
  component: null,
  props: {},
  showModal: () => {},
  hideModal: () => {},
})

export const ModalConsumer = ModalContext.Consumer

export const ModalProvider = ({ children }) => {
  const [state, setState] = useState({
    component: null,
    props: {},
  })

  const showModal = (component, props = {}) => {
    setState({
      ...state,
      component,
      props,
    })
  }

  const hideModal = () => {
    setState({
      ...state,
      component: null,
      props: {},
    })
  }

  return (
    <ModalContext.Provider value={{ ...state, showModal, hideModal }}>
      {children}
    </ModalContext.Provider>
  )
}

export const ModalOpener = ({
  showModal,
  modalProps,
  trigger,
  children,
}) =>
  cloneElement(trigger, {
    onClick: () => showModal(children, modalProps),
  })

export const ModalRoot = () => (
  <ModalConsumer>
    {({ component: Component, props, hideModal }) =>
      Component ? cloneElement(Component, {...props, onClose: hideModal }) : null
    }
  </ModalConsumer>
);
