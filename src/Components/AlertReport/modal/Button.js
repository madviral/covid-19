import React from 'react'
import clsx from 'clsx'

const Button = ({ className, children, onClick }) => (
  <button className={clsx('modal__btn', className)} onClick={onClick}>
    {children}
  </button>
);

export default Button
