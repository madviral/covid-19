import React from 'react'
import clsx from 'clsx'
import { withPortal } from './withPortal'

const Modal = ({ children, className }) => (
  <div className={clsx('modal-cr', className)}>{children}</div>
)

export default withPortal(Modal)
