import { useSpring } from 'react-spring'

export const SLIDE_Y = "SLIDE_Y";
export const POP = "POP";

export const getStyles = (type, { opened, duration }) => {
  switch (type) {
    case SLIDE_Y:
      return () => useSpring({
        from: { transform: 'translate3d(0, 100%, 0)' },
        to: {
          transform: `translate3d(0, ${opened ? 0 : 100}%, 0)`,
        },
        config: { duration },
      })
    case POP:
      return () => useSpring({
        from: { scale: 0.5, opacity: 0 },
        to: {
          scale: 1,
          opacity: opened ? 1 : 0,
        },
        config: { duration },
      })
    default:
      return {}
  }
}
