import React from 'react'
import { animated, useSpring } from 'react-spring'

const Background = ({ duration, opened }) => {

  const styleProps = useSpring({
    from: { opacity: 0 },
    to: { opacity: opened ? 1 : 0 },
    config: { duration },
  })

  return <animated.div style={styleProps} className='modal__background' />
}

Background.defaultProps = {
  duration: 150,
  opened: false,
}

export default Background
