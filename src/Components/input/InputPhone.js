import React from "react";
import InputMask from "react-input-mask";

const InputPhone = ({ id, mask, value, placeholder, required, onChange }) => {
  return (
    <InputMask
      mask={mask}
      onChange={onChange}
      placeholder={placeholder}
      value={value}
    >
      {inputProps => (
        <input
          {...inputProps}
          id={id}
          type="phone"
          className="form-control"
          name={id}
          value={value}
          required={required}
        />
      )}
    </InputMask>
  );
};

InputPhone.defaultProps = {
  mask: "+7 999 999 99 99"
};

export default InputPhone;
