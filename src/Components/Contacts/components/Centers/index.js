import React from 'react';
import clsx from 'clsx';
import { withTranslation } from 'react-i18next';

const Centers = ({ centers, t }) => (
  <section className="page-section">
    <h4 className="page-section__title --bigger">{t('whereToTestCovid19')}</h4>
    <div className="page-section__content">
      {centers.map((cn, i) => (
        <div className={clsx("address-widget", cn.contacts.length === 0 && "--marker")} key={`address.key.${cn.id}.${cn.name}`}>
          {cn.contacts.map((con, i) => (
            <a className='btn call-link' href={`tel:${con.length > 4 ? '+' : ''}${con}`} key={`contacts.key.${con}`}>
              <span className="call-link__num">
                {con.length > 4 ? '+' : ''}
                {`${con.slice(0, 1)} ${con.slice(1, 4)} `}
                <b>
                  {`${con.slice(4, 7)} ${con.slice(7, 9)} ${con.slice(9, 11)}`}
                </b>
              </span>
            </a>
          ))}
          <div className="address-widget__street">{cn.address}</div>
          <div className="address-widget__title">
            {cn.name}
          </div>
        </div>
      ))}
    </div>
    <button className="btn full-width-sm invisible">
      {t('watchOnMap')}
    </button>
  </section>
)

export default withTranslation()(Centers)
