import React from 'react';
import { withTranslation } from "react-i18next";
import { Select } from '../../../Select'
import './styles.css';

const Header = ({ cities, current, setCurrent, t }) => (
  <div className='contacts__header'>
    <h2 className="title">{t('contacts')}</h2>
    <div className="contacts__header-select">
      <Select
        value={current}
        onChange={city => setCurrent(city.value)}
        options={cities.map(city => ({ value: city, label: city }))}
      />
    </div>
  </div>
);

export default withTranslation()(Header)
