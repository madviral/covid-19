import React from 'react';
import { withTranslation } from "react-i18next";

const ContactsInfo = ({ t }) => (
  <section className="page-section">
    <h4 className="page-section__title">{t('emergencyLinePhones')}:</h4>
      <div className="page-section__content">
        <a className="btn call-link" href="tel:1406">
          14 06
        </a>
      </div>
  </section>
);

export default withTranslation()(ContactsInfo)
