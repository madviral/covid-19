export default {
  cities: [
    {
      id: 1,
      name: 'Almaty',
      contacts: ['+7 777 123 45 67', '+9 747 123 11 00', '+1 111 111 11 11'],
      centers: [
        {
          id: 12,
          name: 'Almaty Center',
          address: '1 Facebook way 95661',
          coordinates: {
            lat: 100,
            lng: 200,
          },
          contacts: ['+9 999 999 11 11', '+0 000 000 00 00']
        },
        {
          id: 12,
          name: 'Almaty Center#2',
          address: '2 Facebook way 95661',
          coordinates: {
            lat: 300,
            lng: 200,
          },
          contacts: ['+1 999 999 11 11', '+0 000 000 00 00']
        }
      ]
    },
    {
      id: 2,
      name: 'Nur-Sultan',
      contacts: ['+7 777 777 77 77', '+9 999 999 99 99'],
      centers: [
        {
          id: 22,
          name: 'Nursultan Center',
          address: '1 Facebook way 95661',
          coordinates: {
            lat: 100,
            lng: 200,
          },
          contacts: ['+9 999 999 11 11', '+0 000 000 00 00']
        },
        {
          id: 23,
          name: 'Nursultan Center#2',
          address: '1 Facebook way 95661',
          coordinates: {
            lat: 200,
            lng: 200,
          },
          contacts: ['+4 999 999 11 11', '+1 000 000 00 00']
        },
      ]
    },
  ]
};
