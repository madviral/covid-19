import React, { useEffect, useState } from "react";
import AlertReport from '../AlertReport'
import { getContacts } from '../../api'
import { Header, ContactsInfo, Centers } from './components';

const Contacts = ({ time }) => {
  const [data, setData] = useState(null);
  const [current, setCurrent] = useState('');
  useEffect(() => {
    getContacts.then(res => {
      const contacts = res.data.contacts.sort((a, b) => a.id - b.id)
      setData({ contacts })
      setCurrent(contacts[0].name)
    });
  }, []);
  
  if (!data) return null;

  // filter out data for components
  const cities = data.contacts.map(c => c.name);
  const currentIndex = Math.max(data.contacts.findIndex(contact => contact.name === current), 0)
  const contacts = data.contacts[currentIndex].contacts;
  const centers = data.contacts[currentIndex].centers;

  return (
    <div className="news page">
      <div className="container">
        <div className="date-wrapper">
          <div className="date-title">{time}</div>
        </div>
        <Header cities={cities} current={current} setCurrent={setCurrent} />
        <AlertReport />
        <ContactsInfo contacts={contacts} />
        {
          centers.length > 0 && <Centers centers={centers} />
        }
      </div>
    </div>
  );
};

export default Contacts;
