import React, { useState, useEffect, useRef } from "react"
import { withTranslation } from "react-i18next";
import  { withRouter } from "react-router"
import { shareOrCopy } from "../../utils";
import GroupAdd from "../../icons/GroupAdd";
import CloseIcon from "../../icons/CloseIcon";
import {API} from "../../utils";
import axios from "../../axiosConfig";
import Message from "./Message";
import "./styles.css"

const Chat = ({ match, t, history }) => {
    const { params: { id } } = match;
    const [message, setMessage] = useState("");
    const webSocket = useRef(null);
    const messagesEnd = useRef(null);
    const [messagesList, setMessagesList] = useState([]);
    const [room, setRoom] = useState(null);
    const loadData = async (roomId) => {
        const { data } = await axios({
            method: "GET",
            url: `${API}/chat/api/v1/room/${roomId}`
        });

        setRoom(data)
    };

    useEffect(() => {
        if (typeof id !== 'undefined') {
            const basePath = "wss" + API.slice(5);
            webSocket.current = new WebSocket(basePath + "/chat/api/v1/room-ws/" + id);
            webSocket.current.onmessage = (message) => {
                const messages = message.data.split('\n').map(msg => ({
                    text: msg,
                    title: getRandomName()
                }));
                setMessagesList(prev => [...prev, ...messages]);
            };
            webSocket.current.onclose = () => {
                setMessagesList(prev => [...prev, {
                    text: "Connection closed",
                    title: getRandomName()
                }]);
            };

            return () => {
                console.log('Closed from client');
                webSocket.current.close()
            };
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const scrollToBottom = () => {
        messagesEnd.current.scrollIntoView();
    };

    useEffect(() => {
        scrollToBottom()
    }, [messagesList]);

    useEffect(() => {
        if (typeof id !== 'undefined') {
            loadData(id);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const goToRooms = () => {
        history.push('/leisure/rooms')
    };

    const shareLink = () => {
        shareOrCopy("COVID19-Chat", "Visit my chat at", window.location.href)
    };

    const handleSendMessage = () => {
        if (message) {
            webSocket.current.send(message);
            setMessage("")
        }
    };

    const handleKeyDown = e => {
        if (e.key === 'Enter') {
            handleSendMessage()
        }
    };

    const randomNames = [
        t('jerboa'),
        t('vulture'),
        t('hawk'),
        t('cheetah'),
        t('buzzard'),
        t('albatross'),
        t('badger'),
        t('chipmunk'),
        t('pheasant'),
        t('hedgehog'),
        t('swift'),
        t('sparrow'),
        t('leopard'),
        t('goose'),
        t('wolf'),
        t('bear'),
        t('robin'),
        t('swallow'),
        t('cat'),
        t('deer'),
    ];

    const getRandomName = () => {
        return randomNames[Math.floor(Math.random()*(19))]
    };
    return (<section className="chat">
        <header className="chat__header">
            <div className="chat__header-left">
                {room && room.img && <img className="chat__header-img" src={room.img} alt="RoomImg"/>}
                {room && room.title && <p className="chat__header-title">{room.title}</p>}
            </div>
            <div className="chat__header-right">
                <button className="chat__header-share-button" onClick={shareLink}>
                    <GroupAdd />
                </button>
                <button className="chat__header-close-button" onClick={goToRooms}>
                    <CloseIcon />
                </button>
            </div>
        </header>
        <div className="chat__messages-wrapper">
            <div className="chat__messages">
                <div className="chat__welcome-message">
                    <div className="chat__message-wrapper">
                        <p className="chat__message"><b>{t('welcome') + '!'}</b></p>
                        <p className="chat__message">{t('itIsYourRoom') + '.'}</p>
                        <p className="chat__message">{t('enjoyYourQuarantine') + '!'}</p>
                    </div>
                    <img className="chat__welcome-message-img" src="/images/friends.png" alt="friends"/>
                </div>
                <button className="chat__invite-friends-button" onClick={shareLink}>
                    <GroupAdd color="#FFFFFF" />
                    <p className="chat__invite-friends-button-text">{t('inviteFriendsToChat')}</p>
                </button>
                {messagesList.map((msg,idx) => <Message key={`message-${idx}`} {...msg} />)}
                <div style={{ float:"left", clear: "both" }}
                     ref={(el) => { messagesEnd.current = el; }}>
                </div>
            </div>
            <div className="chat__input-wrapper">
                <input
                    type="text"
                    className="chat__input"
                    placeholder={t('message') + "..."}
                    value={message}
                    onChange={e => setMessage(e.target.value)}
                    onKeyDown={handleKeyDown}
                />
                <button className="chat__send-button" onClick={handleSendMessage}>
                    {t('send')}
                </button>
            </div>
        </div>
    </section>)
};

export default withTranslation()(withRouter(Chat))
