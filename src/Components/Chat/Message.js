import React from "react"
import clsx from "clsx";
import { isYoutubeLink, getIframeSrc } from "./youtubeUtils";
import "./styles.css"

const Message = ({ text, title }) => {
    if (isYoutubeLink(text)) {
        const iframeSrc = getIframeSrc(text);
        return (
            <div className={clsx("chat__message-wrapper", "chat__message-wrapper--full-width")}>
                <p className="chat__message-nickname">{title}</p>
                <a
                    className="chat__youtube-link"
                    href={text}
                    rel="noopener noreferrer"
                    target="_blank"
                >
                    {text}
                </a>
                <iframe
                    title={text}
                    width="100%"
                    height="204"
                    src={iframeSrc}
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                />
            </div>
        )
    }


    return (
        <div className="chat__message-wrapper">
            <p className="chat__message-nickname">{title}</p>
            <p className="chat__message">{text}</p>
        </div>
    )
};

export default Message
