export const youtubeLinkSubstrings = ["youtube", "youtu.be"];

export const isYoutubeLink = text => youtubeLinkSubstrings.some(str => text.indexOf(str) !== -1);

export const getIframeSrc = text => {
    let videoId = "";
    let additionalPropsIndex;

    if (text.indexOf("&") !== -1) {
        additionalPropsIndex = text.indexOf("&")
    }

    if (text.indexOf("youtube") !== -1) {
        videoId = text.slice(text.indexOf("watch?v=") + 8, additionalPropsIndex);
    }

    if (text.indexOf("youtu.be") !== -1) {
        videoId = text.slice(text.indexOf("youtu.be") + 9, additionalPropsIndex)
    }


    return "https://www.youtube.com/embed/" + videoId
};
