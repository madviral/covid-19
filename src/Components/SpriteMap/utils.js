export const getMarkerClassName = (cityID) => {
  const prefix = '--'
  switch (cityID) {
    case 1:
      return `${prefix}almaty ${prefix}grande`
    case 2:
      return `${prefix}astana ${prefix}grande`
    case 4:
      return `${prefix}atyrau`
    case 5:
      return `${prefix}shymkent`
    case 6:
      return `${prefix}taraz`
    case 7:
      return `${prefix}piter`
    case 8:
      return `${prefix}aktau`
    case 13:
      return `${prefix}turkestan`
    case 14:
      return `${prefix}aktobe`
    case 15:
      return `${prefix}pavlo`
    case 16:
      return `${prefix}kizilorda`
    case 18:
      return `${prefix}kostanay`
    case 19:
      return `${prefix}oskemen`
    case 20:
      return `${prefix}uralsk`
    case 21:
      return `${prefix}krg`
    default:
      return ''
  }
}

export const mergeAlaAst = cities => {
  if (cities.length === 0) {
    return []
  }
  const [ala] = cities.filter(c => c.id === 1)
  const [alaObl] = cities.filter(c => c.id === 3)
  const [ast] = cities.filter(c => c.id === 2)
  const [akmObl] = cities.filter(c => c.id === 17)
  const filteredCities = cities
    .filter(c => c.id !== 1)
    .filter(c => c.id !== 2)
    .filter(c => c.id !== 3)
    .filter(c => c.id !== 17)
    .filter(c => c.infected > 0)

  return [
    {
      ...ala,
      infected: ala.infected + alaObl.infected
    },
    ast
      ? {
        ...ast,
        infected: ast?.infected + (akmObl ? akmObl.infected : 0)
      }
    : {},
    ...filteredCities
  ]
}
