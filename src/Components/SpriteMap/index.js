import React from 'react'
import clsx from 'clsx'
import './styles.css'
import { getMarkerClassName, mergeAlaAst } from './utils'

const SpriteMap = ({ cities, mobile, onClick }) => {
  const mergedCities = mergeAlaAst(cities)
  
  return (
    <div className={`sprite-map ${mobile && '--mobile'}`}>
      <div className="sprite-map__map loop" onClick={onClick}>
        {
          mergedCities.map(city => (
            <div
              key={city.id}
              className={clsx('sprite-map__marker', getMarkerClassName(city.id))}
            >
              {city.infected}
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default SpriteMap
