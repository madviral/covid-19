import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import i18n from "i18next";
import cookie from "js-cookie";
import "./App.css";
import { Router, Route, Switch, Link } from "react-router-dom";
import { createBrowserHistory as createHistory } from 'history'
//Components
import LazyLoad from "./Components/LazyLoad"
import Main from "./Components/Main";
import Footer from "./Components/Footer";
import NavFooter from "./Components/NavFooter";
import Graphs from "./Components/Graphs";
import News from "./Components/News";
import Contacts from "./Components/Contacts";
import Leisure from "./Components/Leisure";

const history = createHistory()

const LazyBrainWars = props => (
  <LazyLoad
    component={React.lazy(() => import('./Components/BrainWars'))}
    {...props}
  />
)

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleString("Ru-ru", {
        year: "numeric",
        month: "numeric",
        day: "numeric"
      })
    };
  }

  changeLang(lang) {
    i18n.changeLanguage(lang);
    cookie.set("next-i18next", lang);
    window.location.reload()
  }

  componentDidMount() {
    if (window) {
      window.changeLang = this.changeLang;
    }
  }

  render() {
    const { t } = this.props;
    return (
      <div className="App">
        <Router history={history}>
          <main className="main">
            <Switch>
              <div>
                <nav className="navbar navbar-expand-lg fixed-top">
                  <div className="container">
                    <div className="link-wrapper">
                      <Link className="nav-link link-white" to="/">
                        {t("covid19Kz")}
                      </Link>
                      <Link className="nav-link link-white" to="/news">
                        {t("info")}
                      </Link>
                      <Link className="nav-link link-white" to="/graphs">
                        {t('infographics')}
                      </Link>
                      <Link className="nav-link link-white" to="/contacts">
                        {t("contacts")}
                      </Link>
                    </div>
                    <select
                      className={"desktop_languages_select"}
                      value={i18n.language}
                      onChange={e => this.changeLang(e.target.value)}
                    >
                      <option value="kz">ҚАЗАҚША</option>
                      <option value="ru">РУССКИЙ</option>
                    </select>
                  </div>
                </nav>

                <Route exact path="/">
                  <Main time={this.state.time} />
                </Route>
                <Route exact path="/contacts" component={Contacts} />
                <Route exact path="/graphs" component={Graphs} />
                <Route path="/news" component={News} />
                <Route path="/leisure" component={Leisure} />
                <Route path="/brain-wars" component={LazyBrainWars} />
              </div>
            </Switch>
          </main>
          <NavFooter />
          <Footer />
        </Router>
      </div>
    );
  }
}

export default withTranslation()(App);
