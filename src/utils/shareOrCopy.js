import i18next from 'i18next'
import { copyToClipboard } from './copyToClipboard'

export const shareOrCopy = (title, text, url) => {
    if (navigator && navigator.share) {
        navigator.share({
            title,
            text,
            url,
        })
    } else {
        copyToClipboard(url);
        alert(i18next.t('copied') + '!')
    }
};
