import { format, parseISO } from "date-fns";

export const getDateWithTime = (dateStr, locale) =>
  locale === 'ru'
    ? format(parseISO(dateStr), "dd.MM.yyyy в HH:mm")
    : format(parseISO(dateStr), "dd.MM.yyyy, уақыты HH:mm")

  export const getHoursWithTime = dateStr =>
  format(parseISO(dateStr), "HH:mm");