import axios  from 'axios'
import i18next from "i18next";

const localesForBackend = {
    "ru": "ru",
    "kz": "kk"
}

const axiosConfig = axios.create({})

axiosConfig.interceptors.request.use((config) => {
    const langKey = i18next.language;
    config.headers.locale =  localesForBackend[langKey];

    return config;
});

export default axiosConfig
